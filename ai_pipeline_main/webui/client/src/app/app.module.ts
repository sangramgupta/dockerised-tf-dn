import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { AppComponent } from './app.component';
import { HttpClientModule } from '@angular/common/http';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { MatDialogModule, MatMenuModule, MatInputModule, MatButtonModule, MatIconModule, MatProgressSpinnerModule,
  MatCardModule, MatSidenavModule,MatRadioModule,MatTableModule, MatSliderModule,MatTabsModule,
   MatChipsModule, MatProgressBarModule,  MatListModule,MatSelectModule,
   MatDividerModule,MatCheckboxModule,MatButtonToggleModule, MatSlideToggleModule} from '@angular/material';
import { InstructionsComponent } from './instructions/instructions.component';
import { TrainingJobComponent } from './training-job/training-job.component';
import { SlideshowModule } from '../../node_modules/ng-simple-slideshow';
import { InferenceJobComponent } from './inference-job/inference-job.component';
import { TrainFormSuccessComponent } from './train-form-success/train-form-success.component';
import { DatasetCreatorComponent } from './dataset-creator/dataset-creator.component';


@NgModule({
  declarations: [
    AppComponent,
    InstructionsComponent,
    TrainingJobComponent,
    InferenceJobComponent,
    TrainFormSuccessComponent,
    DatasetCreatorComponent,
  ],
  imports: [
    SlideshowModule,
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    MatSliderModule,
    MatDialogModule,
    MatIconModule,
    MatCheckboxModule,
    MatInputModule,
    MatButtonModule,
    MatButtonToggleModule,
    MatCardModule,
    MatSlideToggleModule,
    MatTabsModule,
    MatInputModule,
    MatDividerModule,
    FormsModule,
    ReactiveFormsModule,
    MatSelectModule,
    MatListModule,
    HttpClientModule,
  ],
  providers: [],
  bootstrap: [AppComponent],
  entryComponents: [InstructionsComponent, TrainFormSuccessComponent]
})
export class AppModule { }
