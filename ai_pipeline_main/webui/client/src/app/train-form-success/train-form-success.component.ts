import { Component, OnInit,Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA} from "@angular/material";

@Component({
  selector: 'app-train-form-success',
  templateUrl: './train-form-success.component.html',
  styleUrls: ['./train-form-success.component.css']
})

export class TrainFormSuccessComponent implements OnInit {

  constructor(
    public dialogRef: MatDialogRef< TrainFormSuccessComponent>) {}

  onNoClick(): void {
    this.dialogRef.close();
  }
  ngOnInit() {
  }

}
