import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TrainFormSuccessComponent } from './train-form-success.component';

describe('TrainFormSuccessComponent', () => {
  let component: TrainFormSuccessComponent;
  let fixture: ComponentFixture<TrainFormSuccessComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TrainFormSuccessComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TrainFormSuccessComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
