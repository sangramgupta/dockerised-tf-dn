import { Component, OnInit } from '@angular/core';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { FormGroup, FormControl, FormArray } from '@angular/forms';
import { Validators, FormBuilder } from '@angular/forms';
import {MatDialog, MatDialogConfig} from '@angular/material';

@Component({
  selector: 'app-dataset-creator',
  templateUrl: './dataset-creator.component.html',
  styleUrls: ['./dataset-creator.component.css']
})
export class DatasetCreatorComponent implements OnInit {

  pcName;
  datasetForm: FormGroup;
  creatorMode;
  datasetInfo: any;
  newClass: string="";
  newPlant: string="";
  newUsecase: string="";
  newTag: string="";
  classes;
  plants;
  usecases;
  tags;
  labels;
  images;

  constructor(private fb: FormBuilder, private http:HttpClient, private dialog:MatDialog) {
    this.datasetForm = fb.group({
      creatorMode: ['database', Validators.required],
      classes: [null],
      plants: [null],
      usecases: [null],
      tags: [null]
    })
  }

  getPcName() {
    this.http.get('http://127.0.0.1:4201/pc-name').subscribe(data => {
      this.pcName = data
      console.log(data);
    })
  }

  getDatasetInfo() {
    this.http.get('http://127.0.0.1:4201/dataset-information').subscribe(data => {
      this.datasetInfo = data;
      console.log(this.datasetInfo);
    })
  }

  addClass() {
    this.http.get('http://127.0.0.1:4201/dataset-information?class=' + this.newClass).subscribe(res => {
      // this.classMsg = res;
    })
    this.getDatasetInfo();
  }

  addPlant() {
    this.http.get('http://127.0.0.1:4201/dataset-information?plant=' + this.newPlant).subscribe(res => {
      // this.plantMsg = res;
    })
    this.getDatasetInfo();
  }

  addUsecase() {
    this.http.get('http://127.0.0.1:4201/dataset-information?usecase=' + this.newUsecase).subscribe(res => {
      // this.usecaseMsg = res;
    })
    this.getDatasetInfo();
  }

  addTag(){}

  onImgChanged(event) {
    this.images = event.target.files;
  }

  onLblChanged(event) {
    this.labels = event.target.files;
  }

  imgUpload() {
    const fd = new FormData();
    for (let file of this.images) {
      fd.append('image', file, file.name)
    }
    console.log(fd);
    this.http.post('http://127.0.0.1:4201/dataset-upload', fd).subscribe(res => {console.log(res);})
  }

  lblUpload() {
    const fd = new FormData();
    for (let file of this.labels) {
      fd.append('label', file, file.name)
    }
    console.log(fd);
    this.http.post('http://127.0.0.1:4201/dataset-upload', fd).subscribe(res => {console.log(res);})
  }


  onSubmit() {
    if (this.images) {
      this.imgUpload()
    }
    if (this.labels) {
      this.lblUpload()
    }
  }

  onClear() {
    this.datasetForm.reset();
  }

  ngOnInit() {
    this.getDatasetInfo();
    this.getPcName();
  }

}
