import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DatasetCreatorComponent } from './dataset-creator.component';

describe('DatasetCreatorComponent', () => {
  let component: DatasetCreatorComponent;
  let fixture: ComponentFixture<DatasetCreatorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DatasetCreatorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DatasetCreatorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
