import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InferenceJobComponent } from './inference-job.component';

describe('InferenceJobComponent', () => {
  let component: InferenceJobComponent;
  let fixture: ComponentFixture<InferenceJobComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InferenceJobComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InferenceJobComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
