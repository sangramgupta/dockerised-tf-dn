import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { FormGroup, FormControl, FormArray } from '@angular/forms';
import { Validators, FormBuilder } from '@angular/forms';
import { NgModule } from '@angular/core';
import { IImage } from '../../../node_modules/ng-simple-slideshow/src/app/modules/slideshow/IImage'

@Component({
  selector: 'app-inference-job',
  templateUrl: './inference-job.component.html',
  styleUrls: ['./inference-job.component.css']
})

export class InferenceJobComponent implements OnInit {

  httpOptions = {headers: new HttpHeaders({ 'Content-Type': 'application/json' })};
  inferenceForm: FormGroup;
  imageSrc: string;
  infLabels;
  infImages;
  images: any;
  inferenceModels: any;
  numImages: number;
  datasetSet: string;
  dataset: string;
  databaseTags: string;
  runMode: string;

  constructor(private fb: FormBuilder, private http:HttpClient) {
    this.inferenceForm = fb.group({
      imageSrc: [null, Validators.required],
      numImages: [null, [Validators.min(0),Validators.pattern("^[0-9]+$")]],
      datasetSet: [null],
      dataset: [null],
      databaseTags: [null],
      runMode: [null, Validators.required]
    });
  }

  getSlideshowImages(event) {
    this.http.get('http://127.0.0.1:4201/dataset-images?dataset=' + event.value).subscribe(data => {
      this.images = data;
      console.log('Images are:');
    })
  }

  onImgChanged(event) {
    this.infImages = event.target.files;
  }

  onLblChanged(event) {
    this.infLabels = event.target.files;
  }

  imgUpload() {
    const fd = new FormData();
    for (let file of this.infImages) {
      fd.append('image', file, file.name)
    }
    this.http.post('http://127.0.0.1:4201/inference-upload', fd).subscribe(res => {console.log(res);})
  }

  lblUpload() {
    const fd = new FormData();
    for (let file of this.infLabels) {
      fd.append('label', file, file.name)
    }
    this.http.post('http://127.0.0.1:4201/inference-upload', fd).subscribe(res => {console.log(res);})
  }

  getInferenceModels() {
    this.http.get('http://127.0.0.1:4201/inference-models').subscribe(data => {
      this.inferenceModels = data;
    })
  }

  onSubmit() {
    if (this.inferenceForm.valid) {

      if (this.infImages) {
        this.imgUpload()
      }
      if (this.infLabels) {
        this.lblUpload()
      }
      const fd = new FormData();
      for (let key in this.inferenceForm.value) {
          fd.append(key, this.inferenceForm.value[key])
      }
      this.http.post('http://127.0.0.1:4201/inference-form', fd).subscribe(res => {console.log(res);})
    }
    else {
      console.log('Form Invalid')
    }
  }

  onClear() {
    this.inferenceForm.reset();
  }

  ngOnInit() {
    this.getSlideshowImages({"value":"init"});
    this.getInferenceModels();
  }

}
