import { Component } from '@angular/core';
import { Injectable } from '@angular/core';
import {FormControl, Validators, FormGroup} from '@angular/forms';
import {MatDialog, MatDialogConfig} from '@angular/material';
import { InstructionsComponent } from './instructions/instructions.component'

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
@Injectable({ providedIn: InstructionsComponent })
export class AppComponent {


  constructor(private dialog:MatDialog) { }

  ngOnInit() {
  }

 //Open Instructions Dialog
  openInstructions()
  {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.autoFocus = false;
    dialogConfig.height='600px';
    dialogConfig.width='800px';

    this.dialog.open(InstructionsComponent,dialogConfig);

  }

}
