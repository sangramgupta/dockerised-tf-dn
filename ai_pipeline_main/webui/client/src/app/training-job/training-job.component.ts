import { Component, OnInit } from '@angular/core';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { FormGroup, FormControl, FormArray } from '@angular/forms';
import { Validators, FormBuilder } from '@angular/forms';
import {MatDialog, MatDialogConfig} from '@angular/material';
import { NgModule } from '@angular/core';
import { TrainFormSuccessComponent } from '../train-form-success/train-form-success.component'
import { IImage } from '../../../node_modules/ng-simple-slideshow/src/app/modules/slideshow/IImage'

@Component({
  selector: 'app-training-job',
  templateUrl: './training-job.component.html',
  styleUrls: ['./training-job.component.css'],
})
@Injectable({ providedIn: TrainFormSuccessComponent })
export class TrainingJobComponent implements OnInit {


  httpOptions = {headers: new HttpHeaders({ 'Content-Type': 'application/json' })};
  trainingForm: FormGroup;
  availableDatasets: any;
  trainingModels: any;
  availableCheckpoints: any;
  availableConfigs: any;
  images: any;
  params: any;
  pow2 = [1,2,4,8,16,32,64,128,256,512,1024];
  datasetName: string="";
  trainingName: string="";
  modelName: string="";
  configName: string="";
  dataset: string="";
  checkpoint: string="";
  batch: number;
  subdivisions: number;
  steps: number;
  rates: number;
  rateMode: string="";
  rate1: number;
  rate2: number;
  rate3: number;
  step1: number;
  step2: number;
  step3: number;
  decay: number;
  dstep: number;

  constructor(private fb: FormBuilder, private http:HttpClient, private dialog:MatDialog) {
    this.trainingForm = fb.group({
      datasetName: [null, Validators.required],
      trainingName: [null, Validators.required],
      modelName: [null, Validators.required],
      checkpoint: [null, Validators.required],
      configName: [null],
      hyperparameters: fb.group({
        batch: [null, [Validators.min(0),Validators.pattern("^[0-9]+$")]],
        subdivisions: [null, [Validators.min(0),Validators.pattern("^[0-9]+$")]],
        steps: [null,  [Validators.min(0),Validators.pattern("^[0-9]+$")]],
        rates: [null, [Validators.min(0), Validators.max(1), Validators.pattern("^0[.][0-9]+$")]],
        rateMode: [null],
        rate1: [null, [Validators.min(0), Validators.max(1), Validators.pattern("^0[.][0-9]+$")]],
        step1: [null, [Validators.min(0),Validators.pattern("[0-9]+")]],
        rate2: [null, [Validators.min(0), Validators.max(1), Validators.pattern("^0[.][0-9]+$")]],
        step2: [null, [Validators.min(0),Validators.pattern("[0-9]+")]],
        rate3: [null, [Validators.min(0), Validators.max(1), Validators.pattern("^0[.][0-9]+$")]],
        step3: [null, [Validators.min(0),Validators.pattern("[0-9]+")]],
        decay: [null, [Validators.min(0), Validators.max(1), Validators.pattern("^0[.][0-9]+$")]],
        dstep: [null, [Validators.min(0),Validators.pattern("[0-9]+")]],
      }),
    });
  }

  onSubmit() {
    if (this.trainingForm.valid) {
      const fd = new FormData();
      for (let key in this.trainingForm.value){
        if (key == 'hyperparameters') {
          for (let _key in this.trainingForm.value[key]) {
            fd.append(_key, this.trainingForm.value[key][_key])
          }
        }
        else {
          fd.append(key, this.trainingForm.value[key])
        }
      }
      this.http.post('http://127.0.0.1:4201/apiv1/trainingjob', fd).subscribe(res => {
        if (res == 'Form Received') {
          this.onFormReceived();
        }
      })
    }
    else {
      console.log('Form Invalid');
    }
  }

  onFormReceived()
  {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.autoFocus = false;
    dialogConfig.height='200px';
    dialogConfig.width='600px';
    // this.onClear();
    this.dialog.open(TrainFormSuccessComponent,dialogConfig);
  }

  onClear() {
    this.trainingForm.reset();
  }

  setModelConfiguration(value) {
    for (var i = 0; i < this.availableConfigs.length; i++){
      if (this.availableConfigs[i].name == value){
        this.trainingForm.patchValue({
          hyperparameters: {
            batch: null,
            subdivsions: null,
            steps: null,
            rates: null,
            rateMode: null,
            rate1: null,
            rate2: null,
            rate3: null,
            step1: null,
            step2: null,
            step3: null,
            decay: null,
            dstep: null,
          }
        });
        this.trainingForm.patchValue({
          checkpoint: 'default',
          hyperparameters: {
            batch: this.availableConfigs[i]['batch'],
            subdivisions: this.availableConfigs[i]['subdivisions'],
            steps: this.availableConfigs[i]['steps'],
            rates: this.availableConfigs[i]['rates'],
            rateMode: this.availableConfigs[i]['rateMode'],
            rate1: this.availableConfigs[i]['rate1'],
            step1: this.availableConfigs[i]['step1'],
            rate2: this.availableConfigs[i]['rate2'],
            step2: this.availableConfigs[i]['step2'],
            rate3: this.availableConfigs[i]['rate3'],
            step3: this.availableConfigs[i]['step3'],
            decay: this.availableConfigs[i]['decay'],
            dstep: this.availableConfigs[i]['dstep'],
          }
        });
      }
    }

  }

  getModelInformation(event) {

    for (var i = 0; i < this.trainingModels.length; i++){
      if (this.trainingModels[i].name == event.value){
        this.http.get('http://127.0.0.1:4201/apiv1/checkpoints?model=' + event.value.replace(' ','%20') + '&mode=' + this.trainingModels[i].mode).subscribe(data => {
          this.availableCheckpoints = data;
        });
        this.http.get('http://127.0.0.1:4201/apiv1/configurations?model=' + event.value.replace(' ','%20')).subscribe(data => {
          this.availableConfigs = data;
          console.log(this.availableConfigs)
          this.trainingForm.patchValue({
            checkpoint: null,
            configName: null,
            hyperparameters: {
              batch: null,
              subdivisions: null,
              steps: null,
              rates: null,
              rateMode: null,
              rate1: null,
              step1: null,
              rate2: null,
              step2: null,
              rate3: null,
              step3: null,
              decay: null,
              dstep: null,
            }
          });
          this.trainingForm.patchValue({
            checkpoint: 'default',
            configName: 'default'
          });
        })
      }
    }
  }

  onConfigChange() {
    this.trainingForm.get('configName').valueChanges.subscribe(val => {
      this.setModelConfiguration(val);
    });
  }

  getTrainingModels() {
    this.http.get('http://127.0.0.1:4201/apiv1/models').subscribe(data => {
      this.trainingModels = data;
    })
  }

  getSlideshowImages(event) {
    this.http.get('http://127.0.0.1:4201/apiv1/dataset-images?dataset=' + event.value).subscribe(data => {
      this.images = data;
    })
  }

  getAvailableDatasets() {
    this.http.get('http://127.0.0.1:4201/apiv1/datasets').subscribe(data => {
      this.availableDatasets = data;
    })
  }

  ngOnInit() {
    this.getAvailableDatasets();
    this.getTrainingModels();
    // this.getSlideshowImages({"value":"init"});
    this.onConfigChange();


  }
}
