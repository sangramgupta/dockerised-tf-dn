from flask import Flask 
from flask import render_template

from config import DevelopmentConfig, ProductionConfig

app = Flask(__name__);

# Config object defined in a file on its own - separation of concerns
app.config.from_object(DevelopmentConfig);


from app import routes
from app.models import Images, Datasets, Usecases, Plants

from app.upload.routes import mod



app.register_blueprint(mod)



# Make some variables available in flask shell
@app.shell_context_processor	
def make_shell_context():
    return {'Images': Images, 'Datasets': Datasets, 'Usecases': Usecases, 'Plants': Plants}




	


