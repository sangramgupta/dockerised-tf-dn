from flask import (

    render_template,
    flash,
    redirect,
    request,
    send_file,
    url_for

)


from app import app


import random
import os


# TODO: add logging features instead of print outs to stdout : priority (2)


# Get static folder path for use throughout the app
STATIC = os.path.join(app.root_path, "static")
DATASETS_PATH = os.path.join(STATIC, 'datasets')


# Handle 404 errors
@app.errorhandler(404)
def not_found(error):
    return render_template('404.html')


# Home page rendering
@app.route('/')
@app.route('/index')
def index():

    # Get datasets
    datasets = [dataset for dataset in os.listdir(DATASETS_PATH) if os.path.isdir(os.path.join(DATASETS_PATH, dataset))]

    # Render index page
    return render_template('index.html', title='Home', datasets=datasets)


@app.route('/getimage', methods=['GET'])
def getimage():

    # Get datasbae
    dataset = request.args.get('dataset')
    image = request.args.get('image') + '.jpg'
    filename = os.path.join(DATASETS_PATH, dataset, image)

    # Get dataset image contents
    # So if dataset A was requested, fetch the images of dataset A and send
    return send_file(filename, mimetype='image/jpg')


@app.route('/getoverview', methods=['GET'])
def getoverview():

    # Get dataset
    dataset = request.args.get('dataset')

    # Choose random images depending on whats available by scanning file system
    images_path = os.path.join(DATASETS_PATH, dataset, 'images')

    # Create a list of image paths in directory
    files = [url_for('static', filename=os.path.join('datasets', dataset, 'images', file))
             for file in os.listdir(images_path) if os.path.isfile(os.path.join(images_path, file))]
    print(files)

    # Create another list of samples
    max_size = len(files)
    sample_size = (request.args.get('samples') or max_size % 15)
    file_samples = random.sample(files, sample_size)

    # Render html page displaying these different html images
    return render_template('overview.html', dataset=dataset, images=file_samples)


# TODO: Add image browsing endpoint : priority (1)
