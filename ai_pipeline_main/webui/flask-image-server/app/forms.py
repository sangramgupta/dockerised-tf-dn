from flask_wtf import FlaskForm, RecaptchaField

from flask_wtf.file import FileField, FileRequired 


from app.models import *

from wtforms import StringField, PasswordField, BooleanField, SubmitField, SelectField 

from wtforms.validators import DataRequired, Email, ValidationError 


class DatasetUploadForm(FlaskForm):

	dataset_name = StringField('Dataset ', render_kw={"placeholder": "Dataset name"}, validators=[DataRequired()])
	
	plant_name_select = SelectField('Plant ', validators=[DataRequired()])

	usecase_name_select = SelectField('Usecase ', validators=[DataRequired()])


	# TODO: Look into this further : priority (5)
	# Flask WTFORM limitation: Cannot upload directory. Will post using vanilla html. Dirty solution but nothing cleaner in my opinion.
	# file = FileField(validators=[FileRequired()])


	# TODO: Enable recaptcha in final version to prevent erroneous uploads
	# recaptcha = RecaptchaField()

	def populate_fields(self, Plants, Usecases):
		"""Need to load select field choices dynamically from database upon request.
		"""
		plants = [(item.name, item.name) for item in Plants.objects()]
		usecases = [(item.name, item.name) for item in Usecases.objects()]

		self.plant_name_select.choices = plants;
		self.usecase_name_select.choices = usecases;

	# TODO: Check that dataset name does not contain any spaces, suggest replace with underscore: priority (1)
	def validate_dataset_name(self, dataset_name):

		dataset = None;

		try:
			dataset = Datasets.objects.get(name=dataset_name.data);

		except Datasets.DoesNotExist:
			pass

		if dataset is not None:
			raise ValidationError("Dataset name already exists. Please choose another.")


class UsecaseCreationForm(FlaskForm):

	usecase_name = StringField('Usecase', render_kw={"placeholder": "Usecase name"}, validators=[DataRequired()])

	# TODO: Apply same no space checking as in datasetname here :priority (1)
	def validate_usecase_name(self, usecase_name):
		"""Check if usecase already exists in database. If it does, issue validation error.
		
		Raises:
			ValidationError -- Flask-WTF specific error.
		"""
		usecase = None;

		try:
			usecase = Usecases.objects.get(name=usecase_name.data);

		except Usecases.DoesNotExist:
			pass

		if usecase is not None:
			raise ValidationError("Usecase already exists.")

class PlantCreationForm(FlaskForm):

	plant_name = StringField('Plant', render_kw={"placeholder": "Plant name"}, validators=[DataRequired()])

	# TODO: Apply same no space checking as in datasetname here :priority (1)
	def validate_plant_name(self, plant_name):
		"""Check if plant already exists in database. If it does, issue validation error.
		
		Raises:
			ValidationError -- Flask-WTF specific error.
		"""
		plant = None;

		try:
			plant = Plants.objects.get(name=plant_name.data);

		except Plants.DoesNotExist:
			pass # No need to do anything.

		if plant is not None:
			raise ValidationError("Plant already exists.")

class UsecaseCreationForm(FlaskForm):
    usecase_name = StringField('Usecase', render_kw={'placeholder': 'Usecase name'}, validators=[DataRequired()])

    def validate_usecase_name(self, usecase_name):
        """Check if usecase already exists in database. If it does, issue validation error.
        
        Raises:
                ValidationError -- Flask-WTF specific error.
        """
        usecase = None
        try:
            usecase = Usecases.objects.get(name=usecase_name.data)
        except Usecases.DoesNotExist:
            pass

        if usecase is not None:
            raise ValidationError('Usecase already exists.')


class PlantCreationForm(FlaskForm):
    plant_name = StringField('Plant', render_kw={'placeholder': 'Plant name'}, validators=[DataRequired()])

    def validate_plant_name(self, plant_name):
        """Check if plant already exists in database. If it does, issue validation error.
        
        Raises:
                ValidationError -- Flask-WTF specific error.
        """
        plant = None
        try:
            plant = Plants.objects.get(name=plant_name.data)
        except Plants.DoesNotExist:
            pass

        if plant is not None:
            raise ValidationError('Plant already exists.')
