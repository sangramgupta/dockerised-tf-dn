import os

from flask import Blueprint, request, render_template, flash, redirect, url_for

from app import app
from app.models import *
from app.forms import *

from app.upload.dataset_handler.DatasetFileManager import DatasetFileManager
from app.upload.dataset_handler.DatasetRuleChecker import DatasetRuleChecker
from app.upload.dataset_handler.DatasetLabelChecker import DatasetLabelChecker
from app.upload.dataset_handler.DatasetUploader import DatasetUploader 


STATIC = os.path.join(app.root_path, "static");
DATASETS_PATH = os.path.join(STATIC, 'datasets');

mod = Blueprint('upload', __name__)


def handle_errors(error_list):
	"""Helper function to display all errors in a list.
	
	Arguments:
		error_list {String[]} -- List of strings
	
	"""
	if error_list:

		# Print all errors
		for error_message in error_list:
			# print("Flashing error messages from dataset check ... ");
			flash(error_message, category="danger");
		
		# Redirect to upload page
		return redirect(request.url);			
	


@mod.route('/upload', methods=['POST', 'GET'])
def upload():

	# Initialize error variable to handle inappropriate submissions
	error_list = [];

	# Initialize dataset upload form
	dataset_form = DatasetUploadForm();

	# Handle post request
	if request.method == "POST":
		
		# Fetch file from request object, since flask wtforms dont support folder upload yet
		file_list = request.files.getlist("folder")

		# Fetch form values
		dataset_name = dataset_form.dataset_name.data
		plant_name = dataset_form.plant_name_select.data
		usecase_name = dataset_form.usecase_name_select.data

		

		# Check that file is not empty.
		if 'folder' not in request.files:
			flash("File not found.", category="danger")
			return redirect(request.url);
		if file_list[0].filename == "": 
			flash('No files selected', category="danger");
			return redirect(request.url);
		if not file_list[0]: 
			flash("Failed to retrieve any file", category="danger")
			return redirect(request.url);



		else: # Selected folder is not empty

			dataset_file_manager = DatasetFileManager(file_list);
			

			# Check dataset rules from dataset_config.json
			dataset_rules_errors = dataset_file_manager.check_rules(app.config["DATASET_CONFIG_PATH"]);
			handle_errors(dataset_rules_errors);


			# Check labels files syntax and extract ground truths
			dataset_labels_errors = dataset_file_manager.check_labels();
			handle_errors(dataset_labels_errors);


			# Upload dataset
			dataset_upload_errors = dataset_file_manager.upload_dataset(dataset_name, usecase_name, plant_name, DATASETS_PATH);
			handle_errors(dataset_upload_errors);

			if dataset_file_manager.error_list:

				flash("Fix errors and try again.", category="info")
				return redirect("upload");
			
			else:
			
				flash('Dataset upload successful', category="success");
				return redirect('index');


			
		

			


	if request.method == "GET":

		# Populate dataset form fields
		dataset_form.populate_fields(Plants, Usecases)

		return render_template("upload.html", dataset_form=dataset_form);


# TODO: Refactor insertion code to make model fields unique and then catch notunique error :priority (2)
@mod.route('/createusecase', methods=["GET", "POST"])
def createusecase():

	if request.method == "POST":

		# Get plant name
		name = request.form['usecase_name']

		# Check if plant already exists
		try:

			usecase = Usecases.objects.get(name=name)
			if usecase:
				flash("Usecase already exists.", category="danger")
				return redirect(url_for("upload.createusecase"));

		except:
			pass

		# Insert into usecases collection
		usecase = Usecases(name=name)
		usecase.save()

		flash("Usecase added successfully.", category="success")

		return redirect(url_for('upload.upload'))

	# If get request, create form and render template
	usecase_creation_form = UsecaseCreationForm()
	return render_template("create_usecase.html", form=usecase_creation_form)

@mod.route('/createplant', methods=["GET", "POST"])
def createplant():

	if request.method == "POST":

		# Get plant name
		name = request.form['plant_name']

		# Check if plant already exists
		try:

			plant = Plants.objects.get(name=name)
			if plant:
				flash("Plant already exists.", category="danger")
				return redirect(url_for("upload.createplant"));

		except:
			pass

		# Insert into plants collection
		plant = Plants(name=name)
		plant.save()

		flash("Plant added successfully.", category="success")

		return redirect(url_for('upload.upload'))

	# If get request, create form and render template
	plant_creation_form = PlantCreationForm()
	return render_template("create_plant.html", form=plant_creation_form)