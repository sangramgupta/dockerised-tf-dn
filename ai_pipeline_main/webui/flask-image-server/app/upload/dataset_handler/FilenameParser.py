import os
from werkzeug.utils import secure_filename
from app.upload.dataset_handler.FileSystemStructures import Directory, File


# TODO: Refactor out of upload module if needed elsewhere : priority (3)
class FilenameParser:
    """ FilenameParser  
    Contains file name parsing functions that can strip down file names and make sure they are not malicious.
    
    """

    def parse_multiple(file_names):
        parsed_names = []
        for name in file_names:
            name = os.path.normpath(name)
            name_list = name.split(os.sep)
            name_list = [secure_filename(item) for item in name_list]
            parsed_names.append(name_list)

        return parsed_names

    def parse_relational(file_names):
        """Parse filenames while establishing relationships between directories and files.
        
        Returns a list of directories and a list of files, as Directory and File objects, defined 
        inside Directory.py .
        
        Arguments:
                file_names {List} -- List of file names to be parsed
        """
        directories = {}
        files = {}

        for ind, name in enumerate(file_names):
        
            
            name = os.path.normpath(name)
            name_list = name.split(os.sep)
            parent_directory = None
            
            # Check if root folder
            if len(name_list) == 1:
                root = File(name_list[0], name_list[0])
                files[root.name] = root
            
            # Parse parent directory/ies and then create file
            else:
            
                parent_directory = Directory(name_list[0] + '/', name_list[0] + '/')
                if parent_directory.name in directories.keys():
                    parent_directory = directories[parent_directory.name]
                for directory in name_list[1:-1]:
                    temp_dir = Directory(directory, file_names[ind].split(directory)[0], parent_directory)
                    if temp_dir.name in directories.keys():
                        temp_dir = directories[temp_dir.name]
                    if parent_directory:
                        parent_directory.add_child(temp_dir)
                    directories[parent_directory.name] = parent_directory
                    parent_directory = temp_dir

            # Create new file object
            file = File(name_list[-1], name, parent_directory)
            files[file.name] = file

            # Add file as child to parent directory
            if parent_directory:
                parent_directory.add_child(file)
            
            directories[parent_directory.name] = parent_directory

        return (directories, files)