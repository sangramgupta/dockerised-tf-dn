import os, pathlib
import PIL

class OSNode:
    """Class of commonalities between Directory and File objects.
    """

    def __init__(self, name, path):
        self.name = name
        self.path = path

    def __repr__(self):
        return self.name + ':' + self.path

    def __eq__(self, other):
        return self.path == other.path and self.name == other.name

    def __hash__(self):
        return hash(self.name + self.path)


class File(OSNode):

    def __init__(self, name, path, parent=None):
        super().__init__(name, path)
        self.extension = pathlib.Path(path).suffix
        self.parent = parent

    def set_parent(self, parent):
        self.parent = parent


class Directory(OSNode):
    """Directory represents an OS directory, and keeps track of all its children directories/files.
    
    Extends:
            OSNode
    """

    def __init__(self, name, path, parent=None):
        super().__init__(name, path)
        self.parent = parent
        self.content = set([])

    def set_parent(self, parent):
        self.parent = parent

    def add_child(self, child):
        self.content.add(child)

    def equals(self, directory):
        """Check if directory contents are similar
        
        Arguments:
                directory {Directory} -- [description]
        """
        return set(directory.content) == set(self.content)

    def equals_ignore_extension(self, directory):
        directory_1 = set([])
        directory_2 = set([])
        for item in self.content:
            if type(item) is Directory:
                raise TypeError('A directory exists inside ' + self.name + '. Must only include files and no folders.')
            directory_1.add(item.name.replace(item.extension, ''))

        for item in directory.content:
            if type(item) is Directory:
                raise TypeError('A directory exists inside ' + directory.name + '. Must only include files and no folders.')
            directory_2.add(item.name.replace(item.extension, ''))

        return directory_1 == directory_2

    def get_file_ignore_extension(self, file_name):

        for item in self.content:
            if item.name.replace(item.extension, '') == file_name:
                return item


class Image(File):
    """Extension of File class that includes some image properties such as height and width.
    
    Extends:
            File
    """

    def __init__(self, name, path, parent=None):
        super().__init__(name, path, parent)
        self.get_size();

    # Static method
    def is_image(file):
        return file.extension in [".jpg", ".jpeg", ".png"]

    def get_size(self):
        try:
            image = PIL.Image.open(self.path);
            self.width, self.height = image.size
        except:
            raise ValueError('Error reading image in PIL : Check image path : ' + self.path)

# TODO: fill this up : priority (1)
class GroundTruthEntry():

    def __init__(self, line, class_map, label_file_name):
        
        values = line.split()  # Split by space into array of values
        
        if len(values) != 5:
            raise ValueError("Error while parsing label file" + label_file_name + ". Invalid entry, wrong number of arguments.")
        
        self.object_class_id = class_map[int(values[0])] 

        self.abs_x_min = int(values[1])
        self.abs_y_min = int(values[2])
        self.abs_width = int(values[3])
        self.abs_height = int(values[4])


class Label(File):

    def __init__(self, file, data, class_map):

        super().__init__(file.name, file.path, file.parent);

        # Extract ground truth
        self.ground_truth = []
        self.class_map = class_map
        self.extract_ground_truth(data);


    def extract_ground_truth(self, data):
        
        for line in data:
            
            ground_truth_entry = GroundTruthEntry(line, self.class_map, self.name);
            self.ground_truth.append(ground_truth_entry);
