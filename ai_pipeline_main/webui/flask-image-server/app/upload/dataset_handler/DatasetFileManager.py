import os


from app.upload.dataset_handler.DatasetRuleChecker import DatasetRuleChecker 
from app.upload.dataset_handler.DatasetLabelChecker import DatasetLabelChecker
from app.upload.dataset_handler.DatasetUploader import DatasetUploader
from app.upload.dataset_handler.FilenameParser import FilenameParser

class DatasetFileManager():

	def __init__(self, file_list):

		self.error_list = []

		self.file_list = file_list;

		# Extract file names from file storage object list
		self.file_names = [file.filename for file in self.file_list];

		# Create a map of [filename, file storage object] for quick access
		self.file_dict = dict((file.filename.split(os.sep)[-1], file) for file in self.file_list)
		self.directory_objects, self.file_objects = FilenameParser.parse_relational(self.file_names);


	def check_rules(self, dataset_config_path):

		self.dataset_rule_checker = DatasetRuleChecker(dataset_config_path);

		# Extend error list with rule checks
		dataset_rules_errors = self.dataset_rule_checker.check_dataset_rules(self.directory_objects, self.file_objects)
		self.error_list.extend(dataset_rules_errors);

		return dataset_rules_errors;


	def check_labels(self):

		self.dataset_labels_checker = DatasetLabelChecker();

		dataset_labels_errors = self.dataset_labels_checker.check_dataset_labels(self.directory_objects, self.file_objects, self.file_dict);
		self.error_list.extend(dataset_labels_errors);

		return dataset_labels_errors;


	def upload_dataset(self, dataset_name, usecase_name, plant_name, DATASETS_PATH):

		self.dataset_uploader = DatasetUploader(self.file_objects, self.directory_objects, self.file_list, self.file_dict, self.dataset_labels_checker.label_objects);

		dataset_upload_errors = self.dataset_uploader.upload_dataset(dataset_name, usecase_name, plant_name, DATASETS_PATH);
		self.error_list.extend(dataset_upload_errors);

		return dataset_upload_errors;
