import json
import os

from app.upload.dataset_handler.FileSystemStructures import Directory, File, Label

class DatasetLabelChecker:
    """Check all labels for syntax errors, and extract ground truth lists.
    """

    def __init__(self): 

        self.error_list = []
        self.label_objects = {};

    def _read_lines(self, file):

        # Process lines into non binary format, and remove newline characters
        lines = file.readlines()
        
        for i in range(len(lines)):
            lines[i] = " ".join(lines[i].decode('ASCII').split())
        
        # Reset file object pointer
        file.seek(0);

        # Return lines list
        return lines


    def check_dataset_labels(self, directories, files, file_dict):

        file_names = files.keys();

        # Create class map
        classes_file = file_dict['classes.txt']
        class_map = self._read_lines(classes_file)
        
        for file_name in file_names:

            file = files[file_name];

            # Check if file is a label
            if file.parent.name == "labels":

                # Read contents of label
                data = self._read_lines(file_dict[file.name])

                if len(lines) < 1:
                    self.error_list.append("Empty label file encountered: {}".format(file_name))
                        

                # Create label object and append to label object list
                try:
                    label_object = Label(file, data, class_map)
                    
                    # Add label object, using its name without the extension for later use with images
                    self.label_objects[os.path.splitext(file_name)[0]] = label_object;

                except ValueError as err:
                    self.error_list.append("Error encountered while creating label: {}".format(err))
                
        return self.error_list;
