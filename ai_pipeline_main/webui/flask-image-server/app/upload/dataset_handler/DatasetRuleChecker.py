import json
from app.upload.dataset_handler.FileSystemStructures import Directory, File

class DatasetRuleChecker:
    """Check a folder for correct structure that follows the ruleset defined.
    
    Constructor returns a   list of errors, if any, and an empty list otherwise.
    
    Note: this class can be generalized into a base class if any other types of file system checks are needed. 
    
    Variables:
            RULESET {list} -- Set of expected first level directory/filenames.
    """

    def __init__(self, dataset_config_path):
        
        self.error_list = []
        self.dataset_config_path = dataset_config_path;


    def parse_dataset_rules(self):
        """Extract dataset rules from dataset_config file in app root path (see app config).
        
        Loops over json object properties and parses each rule into self.required_directories, self.required_files
        """
        with open(self.dataset_config_path) as config_file:

            self.data = json.load(config_file)
        
        # Parse dataset config to find what folders and files are mandatory
        self.required_directories = []
        self.required_files = []
        
        for key, rule_object in self.data.items():
            if rule_object['type'] == 'directory':
                if key not in self.directory_names:
                    self.error_list.append("Directory '" + key + "' is missing.")
                    break
                self.required_directories.append(key)

            else:
                if rule_object['type'] == 'file':
                    if key not in self.file_names:
                        self.error_list.append("File '" + key + "' is missing.")
                        break
                    self.required_files.append(key)


    def check_dataset_rules(self, directories, files):
    	
        self.file_names = files.keys()
        self.directory_names = directories.keys()

        self.parse_dataset_rules();
        
        # Check that all required files are present
        for file in self.required_files:
            if file not in self.file_names:
                self.error_list.append("'" + file + "' file is required but does not exist, or is empty.")
                break

        # Check that all required directories are present
        for directory in self.required_directories:
            if directory not in self.directory_names:
                self.error_list.append("'" + file + "' directory is required but does not exist, or is empty.")
                break
            else: # If directory is present, check that the file types of all files inside it are acceptable according to the set rules
                
                accepted_types = self.data[directory]['accepted_types']
                directory_obj = directories[directory]
                
                for child in directory_obj.content:
                    if not isinstance(child, File):
                        self.error_list.append("No directory must exist inside the '" + directory + "' directory.")
                        break;
                    else:
                        if child.extension not in accepted_types:
                            self.error_list.append("File '" + child.name + "' at '" + child.path + "' is not of the acceptable types listed in the dataset config." + 'Acceptable types are: ' + str(accepted_types))
                            break
            # If directory is present, and contents equal is present, make sure that the directory contents equal whatever specified directory contents ignoring extension
            if 'contents_equal' in self.data[directory].keys():
                
                # Rule exists, check what directory
                equality_directories = self.data[directory]["contents_equal"];

                for equality_directory in equality_directories:
                
                    if not directories[directory].equals_ignore_extension(directories[equality_directory]):
                
                        self.error_list.append("Directories '" + directory + "' and '" + equality_directory + "' do not have equal contents (ignoring extension).")
                        break

        return self.error_list;
