import json
import os
from io import BytesIO, StringIO
import PIL
from mongoengine.errors import NotUniqueError

from app.upload.dataset_handler.FileSystemStructures import (Directory, File, Label, Image)
from app import app
from app.models import *

class DatasetUploader():
	
	""" Upload images after extracting their metadata from the FileStorage objects sent by the web browser.
	"""
	def __init__(self, file_objects, directory_objects, file_list, file_dict, label_objects):
		
		self.error_list = []
		self.file_objects = file_objects;
		self.directory_objects = directory_objects;
		self.file_dict = file_dict;
		self.label_objects = label_objects;
		self.file_list = file_list;

		self.image_ids = []


	def upload_dataset(self, dataset_name, usecase_name, plant_name, DATASETS_PATH):

		
		# Extract all file and directory names
		file_names = self.file_objects.keys();
		directory_names = self.directory_objects.keys();	

		# Construct dataset directory path
		self.directory_path = os.path.join(DATASETS_PATH, dataset_name)

		# Extract all image file names
		image_file_names = [name for name in file_names if self.file_objects[name].parent.name == "images"];
		
		# Upload directory is DATASETS_PATH

		# Loop over all filestorage objects in file list
		# Save the uploaded dataset on the server filesystem, and if file is an image, extract properties after saving
		for file in self.file_list:
			
			# Extract file name from file_list
			filename = file.filename.split(os.sep)[-1];
			file_object = self.file_objects[filename]

			# Construct directory where file should be saved (ignore root directory since we'll be creating our own)
			file_directory = os.path.join(self.directory_path, os.sep.join(file.filename.split(os.sep)[1:]));

			# Create any missing directories
			file_directory_one_up = os.sep.join(file_directory.split(os.sep)[:-1])
			os.makedirs(file_directory_one_up, exist_ok=True);
			
			# Save file
			# NOTE THAT if any reads on the file were performed, and .seek(0) was not called,
			# result will be an empty file saved at this location. Review file reading mechanisms
			# in programming to understand in case this doesn't make sense. (C++ is clearer with this.)
			file.save(file_directory);


			# Check if file is an image, and create image object using label objects if it is
			if Image.is_image(file_object):

				# Create Image object
				image_object = Image(filename, file_directory);

				# Get corresponding label object, which contains ground truth necessary for insertion into images collection
				label_object = self.label_objects[os.path.splitext(filename)[0]]

				##### Insert image into database

				# Get plant and usecase ids
				plant_id = Plants.objects.get(name=plant_name).pk;
				usecase_id = Usecases.objects.get(name=usecase_name).pk;

				ground_truth_embedded_document_list = []
				for ground_truth in label_object.ground_truth:

					ground_truth_entry = GroundTruthModel(	abs_x_min=ground_truth.abs_x_min,
															abs_y_min=ground_truth.abs_y_min,
															abs_height=ground_truth.abs_height,
															abs_width=ground_truth.abs_width,
															object_class_id=ground_truth.object_class_id);

					ground_truth_embedded_document_list.append(ground_truth_entry);

				image_db_entry = Images(width=image_object.width, height=image_object.height,
										plant_id=plant_id, usecase_ids=[usecase_id], 
										filename=image_object.name, path=image_object.path,
										extension=image_object.extension, ground_truth=ground_truth_embedded_document_list)
				
				try:
				
					image_db_entry.save()
					self.image_ids.append(image_db_entry.pk);
				
				except NotUniqueError:
					self.error_list.append("Image named {} already exists in the database. Check that this is a different image.".format(image_object.name));
					return self.error_list;
				




		# Create dataset entry in database
		dataset_db_entry = Datasets(name=dataset_name, n_images=len(self.image_ids), 
									plant_ids=[plant_id], usecase_ids=[usecase_id], 
									image_ids=self.image_ids)
		try:
			dataset_db_entry.save()
			
		except NotUniqueError:
			self.error_list.append("Dataset with this name already exists. Choose another name.")

		return self.error_list;
	