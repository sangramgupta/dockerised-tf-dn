import mongoengine
from app import app

mongoengine.connect(app.config["DB_NAME"], host=app.config["DB_HOST"], port=app.config["DB_PORT"])



class GroundTruthModel(mongoengine.EmbeddedDocument):

	# Absolute values are the relative values in the labels files scaled up according to image height and width
	abs_x_min = mongoengine.IntField()
	abs_y_min = mongoengine.IntField()
	object_class_id = mongoengine.StringField()
	abs_height = mongoengine.IntField()
	abs_width = mongoengine.IntField()

	def __repr__(self):
		return '< Ground truth for {} >'.format(self.object_class_id);


# TODO: Modify this when inference is done : priority (3)
# class InferenceModel(mongoengine.EmbeddedDocument, GroundTruthModel):

# 	confidence = mongoengine.FloatField()

# 	def __repr__(self):
# 		return '< Inference for {} >'.format(self.object_class_id);


class Images(mongoengine.Document):

	# Object ID automatically provided

	# Image width and height fetched from image metadata
	height = mongoengine.IntField()
	width = mongoengine.IntField()

	# Each image is taken in a single plant
	plant_id = mongoengine.ObjectIdField()

	# Dataset and usecase IDs
	usecase_ids = mongoengine.ListField(mongoengine.ObjectIdField())

	# Detections array: saves results from inferences, used later to calculate accuracy of models.
	# detections = mongoengine.EmbeddedDocumentListField(InferenceModel)

	# File metadata and OS information
	filename = mongoengine.StringField(unique=True)
	path = mongoengine.StringField(unique=True)
	extension = mongoengine.StringField()

	# Ground truth array
	ground_truth = mongoengine.EmbeddedDocumentListField(GroundTruthModel)

	def __repr__(self):
		
		return '< Image {} >'.format(self.pk)



class Usecases(mongoengine.Document):

	name = mongoengine.StringField();

	def __repr__(self):

		return '< Usecase {} >'.format(self.pk)


class Plants(mongoengine.Document):

	name = mongoengine.StringField();

	def __repr__(self):
		
		return '< Plant {} >'.format(self.pk);

class Datasets(mongoengine.Document):

	name = mongoengine.StringField(unique=True);
	
	n_images = mongoengine.IntField();

	plant_ids = mongoengine.ListField(mongoengine.ObjectIdField())
	usecase_ids = mongoengine.ListField(mongoengine.ObjectIdField())
	image_ids = mongoengine.ListField(mongoengine.ObjectIdField())


	def __repr__(self):
		
		return '< Dataset {} >'.format(self.pk)










