import unittest

from app.FilenameParser import FilenameParser

class TestFilenameParser(unittest.TestCase):
	"""Test FilenameParser class functionality.

	
	Use parse_relational on on array of file names.
	Match expected output.
	"""

	def test_parse_relational(self):

		file_names = [
			"placebot_dataset/images/01.jpg", 
			"placebot_dataset/images/02.jpeg",
			"placebot_dataset/images/03.png",
			"placebot_dataset/labels/01.txt",
			"placebot_dataset/labels/02.json",
			"placebot_dataset/classes.txt"
		];
		
		directories, files = FilenameParser.parse_relational(file_names);
		
		print("Directories printout: ", directories);
		print("Files printout: ", files);

		for k, directory in directories.items():
			print(directory.name, ":::")
			print(directory.content);
			print("-----------");

		# TODO: Add test cases to TestFilenameParser : priority (7)


if __name__ == '__main__':

	unittest.main();

