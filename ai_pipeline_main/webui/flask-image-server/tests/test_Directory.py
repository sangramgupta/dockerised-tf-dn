import unittest
from app.FileSystemStructures import (	
		Directory, 
		File
	)


class TestDirectory(unittest.TestCase):
	
	"""Testing Directory class functionality.
	"""

	# Check whether directory A contents equal directory B contents
	def test_Directory_File(self):

		child_file_name = "01.jpeg";
		child_file_path = "datasets/datasetA/01.jpeg";

		child_copy_name = "01.png";
		child_copy_path = "datasets/datasetA/01.png";

		child_2_name = "02.png";
		child_2_path = "datasets/datasetA/02.png";

		parent_file_name = "datasetA";
		parent_file_path = "datasets/datasetA";

		root_file_name = "datasets";
		root_file_path = "datasets";

		child = File(child_file_name, child_file_path);
		child2 = File(child_2_name, child_2_path);
		child_copy = File(child_copy_name, child_copy_path);

		parent = Directory(parent_file_name, parent_file_path);
		root = Directory(root_file_name, root_file_path);

		child.set_parent(parent);
		child2.set_parent(parent);

		parent.add_child(child);
		parent.add_child(child2);

		parent.set_parent(root);
		root.add_child(parent);

		self.assertEqual(child.extension, ".jpeg", "Child has the wrong extension.")

		self.assertEqual(type(child), File, "Child is not an instance of File");

		self.assertEqual(child.parent, parent, "Child parent should return parent object.");
		self.assertEqual(parent.parent, root, "Parent parent should return root object");
		self.assertNotEqual(child.parent, root, "Child parent should not return root object.");
		
		self.assertEqual(root.name, root_file_name, "Root folder name is incorrect");
		self.assertEqual(root.path, root_file_path, "Root folder path is incorrect");

		# print("Root content:", root.content);
		# print("Parent content:", parent.content);


		self.assertEqual(child.extension, '.jpeg', "File extension returned is incorrect.");


		# Testing get_file_ignore_extension method
		test = parent.get_file_ignore_extension('01');		
		self.assertEqual(child, test, ".get_file_ignore_extension() returned different object");


	def test_Image_TrainingImage(self):

		child_file_name = "01.jpeg";
		child_file_path = "datasets/datasetA/01.jpeg";


if __name__ == '__main__':
	unittest.main();

