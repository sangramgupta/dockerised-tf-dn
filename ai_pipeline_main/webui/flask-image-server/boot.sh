#!/bin/sh

source venv/bin/activate

# exec is necessary as docker associates the life of the container to the process running it
# exec triggers the process running the scrip to be replaced by the command given, instead of starting it as a new process
exec gunicorn -b :5000 --access-logfile - --error-logfile - index:app

