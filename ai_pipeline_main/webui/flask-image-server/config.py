import os

BASE_DIR = os.path.abspath(os.path.dirname(__file__))


class BaseConfig(object):

	SECRET_KEY = os.environ.get('SECRET_KEY') or '1ih298e1kabds9182yekhalkcn,fnlqj09wee190ejjslndadljqoue0912ueo';

	# # Specify max upload size
	# MAX_CONTENT_LENGTH = 1024*1024*1024*10;

	DEBUG = False
	TESTING = False
	CSRF_ENABLED = True
	SECRET_KEY = '823a9b472dd3067f787aad0670d861766169060b487edbbb'
	DATASET_CONFIG_PATH = os.path.join(BASE_DIR, "dataset_config.json");

class DevelopmentConfig(BaseConfig):

	DEBUG = True;

	# Make sure the following directory exists
	# Flask will NOT create the directory for you
	
	DB_HOST = "10.180.142.120"
	DB_NAME = "ai_pipeline"
	DB_PORT = 27018



class ProductionConfig(BaseConfig):

	DEBUG = False

	# TODO: Change to nextcloud datasets folder in production
	UPLOAD_FOLDER = "/path/to/datasets"
	DATABASE_URI = ""

class TestingConfig(BaseConfig):
	TESTING=True
