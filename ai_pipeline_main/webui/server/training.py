# import system and other modules
import sys
import os
import shutil
import re
import time
import timeit
from termcolor import colored

# import database modules
from pymongo import MongoClient
from bson.objectid import ObjectId
from collections import namedtuple, OrderedDict
import urllib.request
import json
import pprint
import datetime
import random


# Updated classes
from utils.DatasetParser import DatasetParser
from utils.CheckpointManager import CheckpointManager
from utils.ConfigurationManager import ConfigurationManager

# Old classes that need refactoring
from utils.TrainingManager import TrainingManager

from app import app
from app.models import *


# TODO: See what this is for, and remove somehow :priority (2)
# insert tensorflow/models/research to path
sys.path.insert(0, os.environ['TFR'])

# TODO: Move these into app.config ONCE THERE IS AN APP : priority (1)
# Setup paths from environment variables
AI_PIPELINE_BASE_FOLDER = app.config["AI_PIPELINE_BASE_FOLDER"]
TRAINING_BASE_FOLDER = os.path.join(AI_PIPELINE_BASE_FOLDER, "training")
CHECKPOINTS_FOLDER = os.path.join(AI_PIPELINE_BASE_FOLDER, "checkpoints")

HOME_PATH = os.environ['HOME']
TFO_PATH = os.environ['TFO']
PY_PATH = os.environ['PY']


# darknet_path = os.environ['DNPATH']


def run(job):

    hyperparameters = job['hyperparameters']

    # Setup Dataset
    training_name = job['training_name']
    dataset_path = os.path.join(app.config["DATASET_FOLDER"], job['dataset_name'])

    # Create parser
    dataset_parser = DatasetParser(training_name=training_name,
                                   dataset_path=dataset_path,
                                   pipeline_base_training_path=TRAINING_BASE_FOLDER)

    # Parse dataset: Create record files, and all necessary directories
    dataset_parser.initialize()

    # Fetch checkpoint object from models
    model_object = Models.objects.get(name=job['model_name'])

    # Setup checkpoint, download if necessary, and create necessary folders
    checkpoint_manager = CheckpointManager(model_name=job['model_name'],
                                           checkpoint_type=job['checkpoint_mode'],
                                           checkpoint_url=model_object["checkpoint_url"],
                                           checkpoint_name=model_object["checkpoint_name"],
                                           mode=model_object["mode"],
                                           checkpoint_directory=CHECKPOINTS_FOLDER,
                                           object_detection_path=TFO_PATH)

    checkpoint_path = checkpoint_manager.setup()

    # Setup Training Configuration
    configuration_manager = ConfigurationManager(model_object, dataset_parser, checkpoint_manager, hyperparameters)
    configuration_manager.setup()

    # Create training manager
    training_manager = TrainingManager(dataset_parser=dataset_parser,
                                       checkpoint_manager=checkpoint_manager,
                                       configuration_manager=configuration_manager,
                                       hyperparameters=job["hyperparameters"],
                                       dataset_name=job["dataset_name"])

    training_manager.run_training()
#     training_setup = TrainingSetup()
#     training_setup.AI_PIPELINE_BASE_FOLDER = AI_PIPELINE_BASE_FOLDER
#     training_setup.object_detection_path = object_detection_path
#     # training_setup.darknet_path = darknet_path
#     training_setup.python_interpeter = python_interpeter
#     training_setup.mode = checkpoint["mode"]
#     training_setup.config_path = config_path
#     training_setup.backup_dir = backup_dir
#     training_setup.training_dir = training_dir
#     training_setup.chosen_train_dataset_path = chosen_train_dataset_path
#     training_setup.training_name = training_name
#     training_setup.checkpoint_path = checkpoint_path
#     training_setup.data_path = data_path
#     training_setup.num_classes = num_classes
#     training_setup.model_name = model_name
#     training_setup.dataset_name = job['dataset_name']
#     training_setup.hyperparameters = hyperparameters

#     success = training_setup.run_training()
#     # success = True
#     if success is True:
#         export_dir = training_setup.export_model()
#         training_setup.run_testing()
#     else:
#         log_file = os.path.join(os.path.dirname(training_dir), 'train_log.txt')
#         print(colored('Error occured during training:\n', 'red'))
#         with open(log_file) as f:
#             r = f.readlines()
#         if len(r) > 1:
#             print(r[-2])
#         if len(r) > 0:
#             print(r[-1])
#         print(colored('Check log file at {:s}.'.format(log_file), 'red'))

#     train_log = training_setup.training_logger(success)

#     return success, train_log


# if __name__ == '__main__':

#     job_name = '/home/olimjon/MyGit/ai_pipeline_main/training-job.json'
#     run(job_name)
