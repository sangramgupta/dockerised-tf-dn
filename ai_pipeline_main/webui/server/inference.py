# import system and other modules
import sys, os, shutil, re, time, timeit

# import database modules
from pymongo import MongoClient
from bson.objectid import ObjectId
from collections import namedtuple, OrderedDict
import json, pprint

# import setup classes
from utils.setup_dataset import DatasetSetup
from utils.setup_configuration import ConfigurationSetup
from utils.setup_training import TrainingSetup

# insert tensorflow/models/research to path
sys.path.insert(0,os.environ['TFR'])

# setup paths from environment variables
HOME = os.environ['HOME']
base_path = os.path.join(HOME,'Deep_Learning')
object_detection_path = os.environ['TFO']
python_interpeter = os.environ['PY']
darknet_path = os.environ['DNPATH']

def run():

    print('Running inference...')


if __name__ == '__main__':
    run()
