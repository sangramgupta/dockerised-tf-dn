# Pipeline WebUI

This web interface was developed with Angular 6, and Flask with Python3 and MongoDB for server-side functionality.

## Usage

Assuming all the dependencies (Nginx, Flask, Python3, etc.) are installed, all you have to do to run the interface is the following:

### Production
For use in production, or demo purposes, do the following:
```
# in ai_pipeline/webui/server
python db_api.py
```
Then, open your browser to `localhost/TrainTool`, if you are hosting it locally.
Make sure you have built your application for production first. More on this in the [wiki](https://github.com/FirasZog/ai_pipeline/wiki/WebUI).

### Development
For everyday development use, do the following:
```
# in ai_pipeline/webui/server
python db_api.py
# in ai_pipeline/webui/client
ng serve --o
```

Note: you need to have Angular CLI installed to run `ng serve`.

## Basic Installation

This installation only includes needed packages for Production Usage.
Detailed instructions for Development Usage and installing Tensorflow for GPU, compiling Darknet, installing Nvidia-Dockers, and installing Nodejs environments are available in the [wiki](https://github.com/FirasZog/ai_pipeline/wiki/WebUI).

### Python

As previously stated, configure your environments to work with Python3. All `python` and `pip` commands are assumed to be running with Python3 versions.

Install all needed modules using `requirements.txt`:
```
pip install -r requirements.

# Architecture

This section will discuss the software architecture of the pipeline server. 

## Training

Whenever a training request is POSTed, the server creates an redis-queue job object containing all the request parameters and forwards it to the corresponding training redis-server queue, using whatever name is specified in the app configuration. 

### Side note on job/queue architecture
For performing several tasks concurrently, there exists two solutions mainly, which are multi-threading or multi-processing. Both have their usecases. In case of performing a training job, multi-processing is the correct choice. To manage the created processes and distribute them across multiple development machines, we need some job storage mechanism. A queue is a perfect data structure for this job. First job in is the first job that gets picked for execution. For multiple priority jobs, we can have several queues, each with a different priority. For more details, check out the redis queue documentation. 

### Running a training worker
In order to create and run a worker instance on a machine, all that needs to be done is running: 
``` python training_worker.py``` with the right environment. This picks up the redis queue server URL from the app config. Take care that this is set when moving to production, or else the workers will not be able to find the correct redis server queue to pick up jobs from. 

### Training worker specifics
When a training job is fetched, the following steps take place: 
- Dataset is parsed, and the necessary files and directories are created in the pipeline base folder under ```trainings/<training_name>```.
- Required checkpoints are downloaded if they don't already exist in the pipeline base folder under checkpoints.
- The needed configuration file is generated by importing the default model configuration from the mongoDB models object, and modifying its values so as to integrated the user-requested hyperparameters in the job details.
- A training process is initiated, using tensorflow slim's model_main.py, which parses the configuration file into a training loop and executes it. The output of this process is piped to the job output.
- A tensorboard instance process is launched, and pointed to the correct log file being used by the preceding training process.
- When training finishes, the job ends, the results are saved in MongoDB, and the model is exported to the pipeline base folder under ```trainings/<training_name>/output```.
 
