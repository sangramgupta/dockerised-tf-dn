import os, sys
import cv2 as cv

def load_class_names(names_file):

    with open(names_file, 'r') as f:
        r = f.readlines()
    names = []
    for line in r:
        line = line.rstrip('\n')
        line = line.strip()
        names.append(line)

    return names

def convert_groundtruths(gt_dir, img_dir, tmp_dir, names):

    if os.path.exists(gt_dir) is False:
        print('Groundtruth directory does not exists. Exiting...')
        sys.exit(0)

    if os.path.exists(tmp_dir) is False:
        os.makedirs(tmp_dir)

    img_formats = ['png', 'jpg', 'jpeg']
    all_labels = [file for file in os.listdir(gt_dir) if file.endswith('.txt')]

    for label in all_labels:

        label_path = os.path.join(gt_dir,label)
        # find image file with dynamic image format
        image_name = os.path.join(img_dir, label[0:-3])
        image_path = None
        for format in img_formats:
            if os.path.isfile(image_name + format) is True:
                image_path = image_name + format
                break
        if image_path is None:
            print('Image not found, or image format not supported for label {:s}'.format(label_path))
            sys.exit(0)

        img = cv.imread(image_path)
        height, width = img.shape[:2]
        label_empty = os.stat(label_path).st_size == 0
        if label_empty is False:
            with open(label_path,'r') as f:
                gt = f.readlines()
            converted_label_path = os.path.join(tmp_dir, label)
            f = open(converted_label_path, 'w')
            f.close()
            with open(converted_label_path, 'a') as f:
                for line in gt:
                    line = line.rstrip('\n')
                    line = line.strip().split()
                    if len(line) == 5:
                        ctype = names[int(line[0])]
                        left = round((float(line[1]) - float(line[3])/2)*width)
                        top = round((float(line[2]) - float(line[4])/2)*height)
                        right = round((float(line[1]) + float(line[3])/2)*width)
                        bottom = round((float(line[2]) + float(line[4])/2)*height)
                        new_line = ' '.join(map(str, [ctype, left, top, right, bottom]))
                        f.write(new_line + '\n')
                    else:
                        print('Error in annotation format. Check file {:s}'.format(label_path))
                        sys.exit(0)

    print('Converted ground truth labels.')

def convert_predictions(prd_dir, tmp_dir, names):

    if os.path.exists(prd_dir) is False:
        print('Prediction directory does not exists. Exiting...')
        sys.exit(0)

    if os.path.exists(tmp_dir) is False:
        os.makedirs(tmp_dir)

    all_labels = [file for file in os.listdir(prd_dir) if file.endswith('.txt')]

    for label in all_labels:
        label_path = os.path.join(prd_dir, label)
        with open(label_path, 'r') as f:
            prd = f.readlines()
        converted_label_path = os.path.join(tmp_dir, label)
        f = open(converted_label_path, 'w')
        f.close()
        with open(converted_label_path, 'a') as f:
            for line in prd:
                class_idx = int(line.split(' ')[0].strip())
                conf = float(line.split(' ')[-1].strip())
                class_name = names[class_idx]
                new_line = str(class_name) + ' ' + str(conf) + ' ' + ' '.join(map(str, line.split(' ')[1:-1])) + '\n'
                f.write(new_line)

    print('Converted prediction labels.')

if __name__ == '__main__':

    names_file = '/home/robotics/FYP/Datasets/fg_dataset/names-full.txt'
    groundtruth_dir = '/home/robotics/FYP/Datasets/verb_right/verb_right_200_test/labels'
    image_dir = '/home/robotics/FYP/Datasets/verb_right/verb_right_200_test/images'
    prediction_dir = '/home/robotics/FYP/Inferences/verb_right_20_scratch/test@45000/adjusted_labels/iou0.85'
    groundtruth_tmp_dir = '/home/robotics/FYP/Inferences/verb_right_20_scratch/test@45000/adjusted_labels/iou0.85/gt_tmp'
    prediction_tmp_dir = '/home/robotics/FYP/Inferences/verb_right_20_scratch/test@45000/adjusted_labels/iou0.85/prd_tmp'

    names = load_class_names(names_file)
    convert_groundtruths(groundtruth_dir, image_dir, groundtruth_tmp_dir, names)
    convert_predictions(prediction_dir, prediction_tmp_dir, names)
    print('Done.')
