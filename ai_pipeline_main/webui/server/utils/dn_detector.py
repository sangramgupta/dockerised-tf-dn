import darknet as dn
import cv2 as cv
import numpy as np
import os, random

IMG_FORMAT = tuple(['.jpg','.png','.jpeg'])

def clamp(n, minn, maxn):

    return max(min(maxn, n), minn)

def darknet_load(cfg,data,weights):

    print('Loading network and weights...')
    net = dn.load_net(cfg, weights, 0)
    meta = dn.load_meta(data)

    return net, meta

def darknet_detect(filename, output_dir, net, meta, thresh=0.45, hier_thresh=0.5):

    predictions = dn.detect(net,meta,filename.encode(), thresh, hier_thresh)

    coordinates_new = np.array([])
    if not predictions:
        return coordinates_new

    names = []
    for name in meta.names:
        if name == None:
            break
        else:
            names.append(name)

    colors = [(255,0,0),(0,255,0),(0,0,255),(125,0,255),(0,125,255),(255,125,0),(0,255,125),(125,255,0),(255,0,125)]
    random.seed(0)
    random.shuffle(colors)

    img = cv.imread(filename)
    height, width = img.shape[:2]

    for i in range(len(predictions)):

        ctype = names.index(predictions[i][0])
        left = int(clamp((predictions[i][2][0] - (predictions[i][2][2]/2)),0,width))
        top = int(clamp((predictions[i][2][1] - (predictions[i][2][3]/2)),0,height))
        right = int(clamp((predictions[i][2][0] + (predictions[i][2][2]/2)),0,width))
        bottom = int(clamp((predictions[i][2][1] + (predictions[i][2][3]/2)),0,height))
        top_txt = top-10 if top-10 > 0 else 0
        confidence = predictions[i][1]
        color = colors[ctype] if ctype < len(colors)-1 else (0,0,0)
        img = cv.rectangle(img, (left, top), (right,bottom), color, 2)
        img = cv.putText(img,predictions[i][0].decode('utf-8'),(left,top_txt), cv.FONT_HERSHEY_SIMPLEX, 0.8,(255,255,255),2)
        coordinates = [ctype, left, top, right, bottom, confidence]
        coordinates_new = np.append(coordinates_new,coordinates)

    print('Saving image: {:s}'.format(os.path.join(output_dir,filename.split('/')[-1])), end='\r')
    cv.imwrite(os.path.join(output_dir,filename.split('/')[-1]), img)

    return coordinates_new

def dn_save(image_src,output_dir,cfg,data,weights,thresh=0.45,hier_thresh=0.5):

    net, meta = darknet_load(cfg.encode(),data.encode(),weights.encode())

    expname = rename_dir(os.path.dirname(output_dir),output_dir.split('/')[-1],'_v')
    output_dir = os.path.join(os.path.dirname(output_dir),expname)
    os.makedirs(output_dir)

    image_list = []
    if os.path.exists(image_src):
        if os.path.isdir(image_src):
            image_list = [os.path.join(image_src,file) for file in os.listdir(image_src) if file.endswith(IMG_FORMAT)]
        elif image_src.endswith(IMG_FORMAT):
            image_list.appen(image_src)
        elif image_src.endswith('.txt'):
            with open(image_src,'r') as f:
                r = f.readlines()
                r = list(filter(('').__ne__, r))
            for item in r:
                image_list.append(item.rstrip('\n').strip())

    for file in image_list:
        c = darknet_detect(file,output_dir,net,meta)
        c = np.reshape(c, [int(np.shape(c)[0]/6),6])
        np.savetxt(os.path.join(output_dir,(file.split('/')[-1][0:-3]+'txt')),c,fmt='%d %d %d %d %d %1.2f')

def rename_dir(path,name,sep):

	# change the name of the directory if it exists
	name1 = name.split(sep)[0]
	i = 1
	while os.path.exists(os.path.join(path,name)):
		name = name1 + str(sep) + str(i)
		i = i + 1
	return name

if __name__ == '__main__':

    darknet_save(image_src,output_dir,cfg,data,weights)
