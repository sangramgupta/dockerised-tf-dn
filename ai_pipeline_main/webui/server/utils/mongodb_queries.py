from pymongo import MongoClient
from bson.objectid import ObjectId

from app.models import Datasets

def dataset_classes(dataset_name):
    
    dataset_object = Datasets.objects(name=dataset_name)

    dataset_list = list(dataset_object)

    # print(len(dataset_list))
    print("Dataset name: ", dataset_list[0]['name'])
    
    # dataset_id = dataset_list[0]['pk']
    # print(dataset_id)

    # TODO: Extract class names from database : priority (1)
    class_names = ["KLT", "Grip"]

    print("These are the classes: ", class_names)

    return class_names


if __name__ == '__main__':

    test_dataset = 'placebot_detection_top1000'
    print(dataset_classes(test_dataset))