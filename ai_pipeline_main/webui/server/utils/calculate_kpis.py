import os, random, argparse, shutil
import numpy as np
import cv2 as cv
import matplotlib.pyplot as plt

def calculateIOU(_gBox,_pBox, threshold):

    intersection = calcIntersection(_gBox,_pBox)
    union = calcUnion(_gBox,_pBox,intersection)
    IOU = intersection/union
    out = 0 if IOU<threshold else IOU
    return out

def calcIntersection(_gBox,_pBox):

    dx = max(min(_gBox[2],_pBox[2]) - max(_gBox[0],_pBox[0]),0)
    dy = max(min(_gBox[3],_pBox[3]) - max(_gBox[1],_pBox[1]),0)
    areaIntersection = dx*dy
    return areaIntersection

def calcUnion(_gBox,_pBox,_areaIntersection):

    gArea = (_gBox[2]-_gBox[0])*(_gBox[3]-_gBox[1])
    pArea = (_pBox[2]-_pBox[0])*(_pBox[3]-_pBox[1])
    areaUnion = (gArea+pArea)-_areaIntersection
    return areaUnion

def matchGNDTruth(_array):

    out = -1 if(not np.any(_array)) else np.argmax(_array)
    return out

def distribute_iou(mode, fileGround, filePrediction, iou_thresh=0.5, conf_thresh=0.5, nms_thresh=0.5):

    gFile = open(fileGround, 'r')
    scoreMatrix = []
    confidence = []

    for indexG, gLine in enumerate(gFile):

        gBox = np.asarray(((gLine.strip('\n')).strip().split(' ')[1:]),dtype=float)
        pFile = open(filePrediction, 'r')
        scoreArray = []
        for indexP, pLine in enumerate(pFile):
            if mode == 'YOLO':
                pBox = np.asarray(((pLine.strip('\n')).strip().split(' ')[1:-1]),dtype=float)
                conf = pLine.strip('\n').strip().split(' ')[-1]
            elif mode == 'MA':
                pBox = np.asarray(((pLine.strip('\n')).strip().split(' ')[1:]),dtype=float)
                conf = 0.99
            else:
                pBox = np.asarray(((pLine.strip('\n')).strip().split(' ')[2:]),dtype=float)
                conf = pLine.strip('\n').strip().split(' ')[1]
            if conf_thresh <= float(conf):
                confidence.append(conf)
                score = round(calculateIOU(gBox, pBox, iou_thresh), 5)
                scoreArray = scoreArray + [score]
        if scoreArray != []:
            scoreMatrix = scoreMatrix+[scoreArray]

    if(len(scoreMatrix) != 0):

        scoreMatrix = np.asanyarray(np.transpose(scoreMatrix))
        result = np.apply_along_axis(matchGNDTruth,1, scoreMatrix)
        for index, values in enumerate(result):
            for ind, val in enumerate(result[index+1:]):
                if(values == val and values >= 0 ):
                    result[index+1+ind] = -1
    else:
        result = []

    return result, scoreMatrix, confidence

def calcAP(result, scoreMatrix, confs):

    precision = []
    recall = []
    boundingboxes = []
    TP = 0
    FP = 0
    FN = 0
    tp = 0
    fp = 0
    fn = 0
    IoUs = []
    avg_IoU = 0

    if (len(scoreMatrix) != 0):

        num_of_p = scoreMatrix.shape[0]
        num_of_g = scoreMatrix.shape[1]

        num_of_p -= len(np.argwhere(result == -1))

        FN = num_of_g - num_of_p if (num_of_g > num_of_p) else 0

        for index, value in enumerate(result):
            boundingboxes.append([index, value, scoreMatrix[index, value], confs[index]])
        for items in boundingboxes:
            if (items[1] != -1):
                TP += 1
            else:
                FP += 1

        FN = FN + (num_of_p - TP)
        if FN > 0:
            pass
            #print(FN)

        for items in boundingboxes:
            if (items[1] != -1):
                tp += 1
                precision.append((tp) / float((tp + fp)))
                recall.append((tp) / float((TP + FN)))
                IoUs.append(items[2])
            else:
                fp += 1
                precision.append((tp) / float((tp + fp)))
                recall.append((tp) / float((TP + FN)))

        if len(IoUs)==0:
            avg_IoU = 0
        else:
            avg_IoU = (sum(IoUs))/len(IoUs)
        AP = precision[0]*(recall[0])
        for k in range(1, len(precision)):
            AP = AP + precision[k]*(recall[k]-recall[k-1])
        recall_value = recall[-1]
        precision_value = precision[-1]

    else:
        AP = 0
        recall_value = 0
        precision_value = 0
        avg_IoU = 0

    return AP, recall_value, avg_IoU, TP, FN, FP, precision_value

def calcmAP(array_of_APs):

    mAP = sum(array_of_APs)/float(len(array_of_APs)) if len(array_of_APs) > 0 else 0
    return mAP

def yolo2abs(groundtruth, file, tmp, imagedir):

    # TODO: dynamic image format
    image_name = os.path.join(imagedir,file[0:-3] + 'png')
    if os.path.isfile(image_name) is False:
        image_name = os.path.join(imagedir, file[0:-3] + 'jpg')
    label_name = os.path.join(groundtruth,file)
    bbox = []
    img = cv.imread(image_name)
    height, width = img.shape[:2]
    label_exists = os.path.isfile(label_name)
    label_empty = True if label_exists is not True else os.stat(label_name).st_size == 0
    if (label_exists is False or label_empty is True) is False:
        with open(label_name,'r') as f:
            yolo = f.readlines()
        for line in yolo:
            line = line.rstrip('\n')
            line = line.strip().split()
            ctype = int(line[0])
            left = round((float(line[1]) - float(line[3])/2)*width)
            top = round((float(line[2]) - float(line[4])/2)*height)
            right = round((float(line[1]) + float(line[3])/2)*width)
            bottom = round((float(line[2]) + float(line[4])/2)*height)
            if len(line) == 6:
                bbox.append([ctype, left, top, right, bottom, round(float(box[5]))])
            elif len(line) == 5:
                bbox.append([ctype, left, top, right, bottom])
            else:
                print('Error in annotation format. Check file {:s}'.format(label_name))
                exit()

    bbox = np.asarray(bbox)
    np.savetxt(os.path.join(tmp,file), bbox, fmt='%d %d %d %d %d')
    # return bbox

def plot_fct(expfolder, name, to_plot, ylabel, title, expname):

    plotname = os.path.join(expfolder, name)
    fig = plt.figure()
    plt.plot(to_plot, figure=fig, marker='o', markerfacecolor='blue', markeredgecolor='blue')
    plt.ylabel(ylabel)
    plt.xlabel("Number of images")
    plt.ylim(0.0, 1.0)
    plt.title(title + "Training: " + expname)
    fig.savefig(plotname)
    plt.close(fig)

def calculateKPIs(mode, expname, groundtruth, prediction, expfolder, imagedir, iou_thresh, conf_thresh=0.5):

    expfile = os.path.join(expfolder, "KPIs.txt")
    if not os.path.exists(expfolder):
        os.makedirs(expfolder)

    tmp = os.path.join(groundtruth, 'tmp')
    if os.path.exists(tmp) is not True:
        os.makedirs(tmp)

    AP_values = []
    Recall_per_image = []
    Precision_per_image = []
    F1_per_image = []
    overall_F1 = []
    overall_Recall = []
    overall_Precision = []
    x = 0
    overall_TP = 0
    overall_FN = 0
    overall_FP = 0

    with open(expfile, "a") as myfile:
       	myfile.write("filename, average precision, recall, average IoU\n")

    valid = list(set(os.listdir(prediction)).intersection(os.listdir(groundtruth)))
    for file in valid:
        if os.stat(os.path.join(groundtruth, file)).st_size == 0 or os.stat(os.path.join(prediction, file)).st_size == 0 or not file.endswith('.txt'):
            valid.remove(file)
    if len(valid) == 0:
        return -1, [-1], [-1], [-1], [-1], [-1], [-1], [-1], [-1]
    else:

        for file in valid:

            yolo2abs(groundtruth,file,tmp, imagedir)
            fileGround = os.path.join(tmp, file)
            filePrediction = os.path.join(prediction, file)
            x += 1
            results, matrix, confidences = distribute_iou(mode, fileGround, filePrediction, iou_thresh, conf_thresh)
            AP_value, Recall_value, AVG_IOU_value, TP, FN, FP, Precision_value = calcAP(results, matrix, confidences)
            AP_values.append(AP_value)
            Recall_per_image.append(Recall_value)
            Precision_per_image.append(Precision_value)
            overall_TP += TP
            overall_FN += FN
            overall_FP += FP
            if not (Precision_value == 0 or Recall_value == 0):
                F1_per_image.append(2/float((1/Precision_value) + (1/Recall_value)))

            Total_Precision = (overall_TP) / float((overall_TP + overall_FP))
            Total_Recall = (overall_TP) / float((overall_TP + overall_FN))
            overall_Precision.append(Total_Precision)
            overall_Recall.append(Total_Recall)
            if not (Total_Precision == 0 or Total_Recall == 0):
                overall_F1.append(2/float((1/Total_Precision) + (1/Total_Recall)))
            with open(expfile, "a") as myfile:
            	myfile.write(os.path.splitext(file)[0] + ", " + str(round(AP_value,3)) + ", " + str(round(Recall_value,3)) + ", " + str(round(Total_Recall,3)) + ", " + str(round(AVG_IOU_value,3)) + "\n")

        mAP = calcmAP(AP_values)

        plot_fct(expfolder, "1_AP_ditribution.png", AP_values, "Average Precision per image", "Average Precision, Training: ", expname)
        plot_fct(expfolder, "1_Recall_per image.png", Recall_per_image, "Recall per image", "Recall per image, Trainingl: ", expname)
        plot_fct(expfolder, "1_Precision_per image.png", Precision_per_image, "Precision per image", "Precision per image, Training: ", expname)
        plot_fct(expfolder, "1_F1_per image.png", F1_per_image, "F1 per image", "F1 per image, Training: ", expname)
        plot_fct(expfolder, "1_Recall_Total.png", overall_Recall, "Cumulative Recall", "Cumulative Recall, Training: ", expname)
        plot_fct(expfolder, "1_Precision_Total.png", overall_Precision, "Cumulative Precision", "Cumulative Precision, Training: ", expname)
        plot_fct(expfolder, "1_F1_Total.png", overall_F1, "Cumulative F1", "Cumulative F1, Training: ", expname)

        with open(os.path.join(expfolder, "log.txt"), "a") as logfile:
            logfile.write("Training: " + expname + ", Images: " + str(x) + ", @IOU: " + str(iou_thresh) + " , mAP: " + str(mAP) + ", Recall: " + str(overall_Recall[-1]) + ", Precision: " + str(overall_Precision[-1]) + ", F1: " + str(overall_F1[-1]) + "\n")

        shutil.rmtree(tmp)
        return x, AP_values, mAP, Recall_per_image, overall_Recall, Precision_per_image, overall_Precision, F1_per_image, overall_F1

if __name__ == '__main__':

    mode = 'MA' # 'YOLO' or 'TF'
    expname = "riss_20_scratch"                                                       # model or test name
    groundtruth = "/home/robotics/FYP/Datasets/verb_right/verb_right_200_test/labels"       # folder containing groundtruth labels
    prediction = "/home/robotics/FYP/Trainings/adjusted_labels_model62/predictions/tmp"    # folder containing prediction labels
    expfolder = "/home/robotics/FYP/Trainings/adjusted_labels_model62/predictions/map"            # folder to save to
    imagedir = "/home/robotics/FYP/Datasets/verb_right/verb_right_200_test/images"          # folder containing the images

    iou_thresh = 0.75
    conf_thresh = 0.5

    x, AP_values, mAP, Recall_per_image, overall_Recall, Precision_per_image, overall_Precision,\
                                F1_per_image, overall_F1 = calculateKPIs(mode, expname, groundtruth, prediction, expfolder, imagedir, iou_thresh, conf_thresh)

    print('mAP: {} \n Recall: {} \n Precision: {} \n F1: {} \n number of images: {}'.format(mAP, overall_Recall[-1], overall_Precision[-1], overall_F1[-1], x))
