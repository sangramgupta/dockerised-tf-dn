from bson import CodecOptions, SON
from bson.objectid import ObjectId

import os
import json
import collections
import regex
import re

# from server.models import *


class ConfigurationManager():

    def __init__(self, model_object, dataset_parser, checkpoint_manager, hyperparameters):

        # Environment Configuration
        self.dataset_parser = dataset_parser
        self.checkpoint_manager = checkpoint_manager
        self.hyperparameters = hyperparameters

        self.model_object = model_object

        self.training_name = dataset_parser.training_name
        self.training_dir = dataset_parser.training_directory
        self.dataset_dir = dataset_parser.dataset_path
        self.label_map_path = dataset_parser.label_map_path
        self.num_classes = dataset_parser.n_classes
        self.num_test = dataset_parser.n_training_files
        self.num_train = dataset_parser.n_testing_files
        self.data_directory = dataset_parser.data_directory
        self.backup_path = dataset_parser.backup_directory
        self.default_inputs_path = dataset_parser.default_inputs_path

        self.model_name = checkpoint_manager.model_name
        self.checkpoint_path = checkpoint_manager.checkpoint_path

        # Hyperparameter Configuration
        self.batch_size = hyperparameters['batch']
        self.subdivisions = hyperparameters['subdivisions']
        self.total_steps = hyperparameters['steps']
        self.init_rate = hyperparameters['rates']
        self.rate_mode = hyperparameters['rate_mode']
        self.rate1 = hyperparameters['rate1']
        self.rate2 = hyperparameters['rate2']
        self.rate3 = hyperparameters['rate3']
        self.step1 = hyperparameters['step1']
        self.step2 = hyperparameters['step2']
        self.step3 = hyperparameters['step3']
        self.decay = hyperparameters['decay_rate']
        self.decay_step = hyperparameters['decay_step']

    def create_tf_cfg(self, cfg):

        # Setup paths
        output_train = self.dataset_parser.train_record_file_path
        output_test = self.dataset_parser.test_record_file_path

        # TODO: Refactor file name in case this doesn't affect anything else : priority (2)
        self.config_path = os.path.join(self.training_dir,
                                        self.training_name + '-train.config')

        # Modify configuration file
        cfg["model"][list(cfg["model"].keys())[0]]["num_classes"] = self.num_classes

        if self.checkpoint_path is -1:
            cfg["train_config"]["fine_tune_checkpoint"] = "&&no_checkpoint&&"
            cfg["train_config"]["from_detection_checkpoint"] = "false"
        else:
            cfg["train_config"]["fine_tune_checkpoint"] = ''.join([
                '&&',
                os.path.normpath(self.checkpoint_path), '/model.ckpt&&'
            ])
            cfg["train_config"]["from_detection_checkpoint"] = "true"

        # Read and modify default inputs
        file = open(self.default_inputs_path, 'r')
        default_inputs_contents = file.read()
        file.close()

        inputs = json.loads(default_inputs_contents, object_pairs_hook=collections.OrderedDict)
        inputs['train_input_reader']['tf_record_input_reader']['input_path'] = ''.join(['&&', output_train, '&&'])
        inputs['train_input_reader']["label_map_path"] = ''.join(['&&', self.label_map_path, '&&'])
        inputs['eval_config']['num_examples'] = self.num_test
        inputs['eval_config']['max_evals'] = self.num_test
        inputs['eval_input_reader']['tf_record_input_reader']['input_path'] = ''.join(['&&', output_test, '&&'])
        inputs['eval_input_reader']["label_map_path"] = ''.join(['&&', self.label_map_path, '&&'])

        # Convert JSON objects to a string
        mod = json.dumps(cfg["model"], indent=2)
        mod = ' '.join(['model', mod])
        tr = json.dumps(cfg["train_config"], indent=2)
        tr = ' '.join(['train_config', tr])
        train_input_reader = json.dumps(inputs["train_input_reader"], indent=2)
        train_input_reader = ' '.join(
            ['train_input_reader', train_input_reader])
        eval_config = json.dumps(inputs["eval_config"], indent=2)
        eval_config = ' '.join(['eval_config ', eval_config])
        eval_input_reader = json.dumps(inputs["eval_input_reader"], indent=2)
        eval_input_reader = ' '.join(['eval_input_reader', eval_input_reader])
        cfg = '\n'.join(
            [mod, tr, train_input_reader, eval_config, eval_input_reader])
        cfg = cfg.replace('\"', '')
        cfg = cfg.replace('&&', '\"')

        # Create configuration file
        f = open(self.config_path, 'w')
        f.write(cfg)
        f.close()

        return self.config_path

    def create_yolo_cfg(self, cfg):

        # setup configuration paths
        train_cfg = os.path.join(self.training_dir,
                                 self.training_name + '-train.cfg')

        test_cfg = os.path.join(self.training_dir,
                                self.training_name + '.data')

        train_file = self.dataset_parser.train_file_path
        test_file = self.dataset_parser.test_file_path
        data_directory = self.dataset_parser.data_directory

        data_file = os.path.join(self.dataset_dir, self.training_name + '.data')
        names_file = self.dataset_parser.names_file_path

        # create data file
        f = open(data_file, 'w')
        f.write('classes = {:d}\n'.format(self.num_classes))
        f.write('train = {:s}\n'.format(train_file))
        f.write('valid = {:s}\n'.format(test_file))
        f.write('names = {:s}\n'.format(names_file))
        f.write('backup = {:s}\n'.format(self.backup_path))
        f.write('#eval = coco')
        f.close()

        # modify configuration file
        cfg = cfg["model"]["cfg"]
        cfg = regex.sub('(?<=classes( +)?=( +)?)[0-9]+', str(self.num_classes),
                        cfg)
        num = regex.search('(?<=num( +)?=( +)?)[0-9]+', cfg).group(0)
        cfg = regex.sub(
            '(?<=filters( +)?=( +)?)[0-9]+(?=\nactivation=linear\n\n\[region\])',
            str((self.num_classes + 5) * int(num)), cfg)
        cfg = regex.sub('(?<=batch( +)?=( +)?)[0-9]+', self.batch_size,
                        cfg) if self.batch_size != 'pow' else cfg
        cfg = regex.sub('(?<=subdivisions( +)?=( +)?)[0-9]+',
                        self.subdivisions,
                        cfg) if self.subdivisions != 'pow' else cfg
        cfg = regex.sub('(?<=max_batches( +)?=( +)?)[0-9]+', self.total_steps,
                        cfg) if self.total_steps != 'null' else cfg
        cfg = regex.sub('(?<=learning_rate( +)?=( +)?)[0-9]+', self.init_rate,
                        cfg) if self.init_rate != 'null' else cfg

        steps = [self.step1, self.step2, self.step3]
        scales = [self.rate1, self.rate2, self.rate3]

        steps = list(filter(('null').__ne__, steps))
        scales = list(filter(('null').__ne__, scales))

        if self.rate_mode == 'manual_step_learning_rate':
            if len(steps) > 0 and len(scales) > 0:
                cfg = regex.sub('(?<=policy( +)?=( +)?)[a-z]+', 'steps', cfg)
                cfg = regex.sub('(?<=rate( +)?=( +)?)[0-9]+', self.init_rate, cfg)

        # create configuration files
        cfg = cfg.replace('\\n', '\n')
        f = open(train_cfg, 'w')
        f.write(cfg)
        f.close()
        cfg = regex.sub('(?<=batch( +)?=( +)?)[0-9]+', str(1), cfg)
        cfg = regex.sub('(?<=subdivisions( +)?=( +)?)[0-9]+', str(1), cfg)
        f = open(test_cfg, 'w')
        f.write(cfg)
        f.close()

        return train_cfg, data_file

    def setup(self):

        mode = self.model_object["mode"]
        # Create configurations
        self.data_path = None

        if mode == 'TF':
            self.config_path = self.create_tf_cfg(self.model_object)

        elif mode == 'YOLO':
            self.config_path, self.data_path = self.create_yolo_cfg(self.model_object)

        return self.config_path, self.data_path
