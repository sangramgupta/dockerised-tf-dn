# run_inference
# if inference_truth:
#     run_kpi_calculations
#
# if inference_cfg:
#     darknet_save(inference_images,inference_output,infernce_cfg,inference_data,inference_model)
# else:
#     tf_save(inference_images,inference_output,inference_data,inference_model)
#
#
#     if self.inf_images and self.inf_model and self.inf_label:
#         self.inf_thread = run_inference()
#         self.inf_thread.inf_msg.connect(self.inf_feedback)
#         self.inf_thread.weights = self.inf_model
#         self.inf_thread.data = self.inf_label
#         self.inf_thread.cfg = self.inf_cfg
#         self.inf_thread.filename = self.inf_images
#         export_name = rename_dir(os.path.join(base_path,'Inferences'),'predictions','_v')
#         self.inf_thread.export = os.path.join(base_path, 'Inferences', export_name)
#         dataset_name = os.path.dirname(os.path.dirname(self.inf_images[0]) if type(self.inf_images) is list else os.path.dirname(self.inf_images))
#         # self.inf_thread.export = os.path.dirname(self.inf_images[0]) if type(self.inf_images) is list else os.path.dirname(self.inf_images)
#         # self.inf_thread.export = os.path.join(self.inf_thread.export,rename_dir(self.inf_thread.export,'predictions','_v'))
#         log = {'name': export_name, 'dataset': dataset_name, 'model': os.path.dirname(self.inf_model)}
#         if self.inf_gtruth:
#             self.inf_thread.gtruth = self.inf_gtruth
#         self.inf_thread.start()
#     else:
#         self.inf_feedback('Error. Please upload images or model.','red')

# class run_inference(QThread):
#
#     inf_msg = pyqtSignal(str,str)
#
#     def __init__(self):
#         QThread.__init__(self)
#         self.cfg = None
#         self.weights = None
#         self.data = None
#         self.filename = None
#         self.export = None
#         self.gtruth = None
#
#     def run(self):
#         self.inf_msg.emit('Running inference on image set...','blue')
#         if self.cfg:
#             darknet_save(self.filename,self.export,self.cfg,self.data,self.weights)
#         else:
#             tf_save(self.filename,self.export,self.data,self.weights)
#         self.inf_msg.emit('Detection images and labels saved in {:s}'.format(self.export),'green')
#         print('Detection images and labels saved in {:s}'.format(self.export))
#         if self.gtruth:
#             self.x, self.AP_values, self.mAP = calculatemAP(os.path.dirname(self.export).split('/')[-1], self.weights.split('.')[0], self.gtruth, self.export, os.path.join(self.export,rename_dir(self.export,'mAP','_v')))
#             if self.x == -1:
#                 self.inf_msg.emit('Empty detections or no valid files.','red')
#             else:
#                 self.inf_msg.emit("Mean Average Precision (mAP): {:f}".format(self.mAP),'green')
#                 self.inf_msg.emit("Test complete!",'green')
