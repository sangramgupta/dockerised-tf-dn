import sys
import os
import glob
import shutil
import re
import time
import json
from termcolor import colored
from subprocess import Popen, PIPE, STDOUT
import webbrowser
import signal
import urllib.request

# from .dn_detector import dn_save
# from .tf_detector import tf_save
# from .calculate_kpis import calculateKPIs


class TrainingManager():

    def __init__(self, dataset_parser, checkpoint_manager, configuration_manager, hyperparameters, dataset_name):

        # Paths from DatasetInitializer
        self.dataset_parser = dataset_parser
        self.checkpoint_manager = checkpoint_manager
        self.configuration_manager = configuration_manager

        self.base_path = self.dataset_parser.pipeline_base_training_path
        self.training_dir = self.dataset_parser.training_directory
        self.backup_dir = self.dataset_parser.backup_directory
        self.training_name = self.dataset_parser.training_name
        self.checkpoint_path = self.checkpoint_manager.checkpoint_path

        self.num_classes = self.dataset_parser.n_classes
        self.dataset_dir = self.dataset_parser.dataset_path
        self.dataset_name = dataset_name

        # TODO: YOLO specific
        self.data_path = None

        # Paths from checkpoint setup
        self.model_name = self.checkpoint_manager.model_name
        self.mode = self.checkpoint_manager.mode
        self.config_path = self.configuration_manager.config_path

        # Environment variables
        # TODO: YOLO specific
        self.darknet_path = ""
        self.python_interpeter = os.environ['PY']
        self.object_detection_path = os.environ['TFO']

        # Parameters from training script
        self.hyperparameters = hyperparameters

    # rename directory if duplicate name exists by adding '_v' to the name
    # i.e. 'training_v1', 'training_v2'
    def rename_dir(self, path, name, sep):

        # change the name of the directory if it exists
        name1 = name.split(sep)[0]
        i = 1
        while os.path.exists(os.path.join(path, name)):
            name = name1 + str(sep) + str(i)
            i = i + 1

        return name

    def create_dir(self, path):

        # create directory if it does not exist
        try:
            os.makedirs(path)
        except OSError as e:
            if e.errno != errno.EEXIST:
                raise

    def run_training(self):

        training_command = ""

        if self.mode == 'YOLO':

            # TODO: Setup more advanced GPU configuration, depending on computer environment variables : priority (10)
            yolo_gpu_cmd = '-gpus 0'
            yolo_training_command = '{:s}/darknet detector train {:s} {:s} {:s} {:s}'.format(
                self.darknet_path, self.data_path, self.config_path,
                self.checkpoint_path, yolo_gpu_cmd)
            training_command = yolo_training_command

        elif self.mode == 'TF':

            # TODO: Refactor to use model main internals? : priority (2)
            # Create training command using TFO, config file, backup directory
            tf_gpu_cmd = 'export CUDA_VISIBLE_DEVICES=0'

            """Command description from TFO/model_main.py
            
                @param: {str} model_dir, default=None : Path to output model directory where event and checkpoint files will be written.
                @param: {int} num_train_steps, default=None : Number of train steps.
                @param: {bool} eval_training_data, default=False: If training data should be evaluated for this job. Note that one call only use this in eval-only mode, and `checkpoint_dir` must be supplied.
                @param: {int} sample_1_of_n_eval_examples, default=1 : Will sample one of every n eval input examples, where n is provided.
                @param: {int} sample_1_of_n_eval_on_train_examples, default=5 : Will sample one of every n train input examples for evaluation, where n is provided. This is only used if `eval_training_data` is True.
                @param: {str} hparams_overrides, default=None : Hyperparameter overrides, represented as a string containing comma-separated hparam_name=value pairs.
                @param: {str} checkpoint_dir, default=None : Path to directory holding a checkpoint.  If `checkpoint_dir` is provided, this binary operates in eval-only mode, writing resulting metrics to `model_dir`.
                @param: {bool} run_once, default=False : If running in eval-only mode, whether to run just one round of eval vs running continuously (default).                
            """
            tf_training_command = self.python_interpeter + ' {:s}model_main.py --alsologtostderr --pipeline_config_path={:s} --model_dir={:s}'.format(
                self.object_detection_path, self.config_path, self.backup_dir)

            training_command = ''.join([tf_gpu_cmd, '&&', tf_training_command])

            # TODO: Setup tensorboard : priority (2)
            # kill tensorboard if it is already running
            # self.kill_process('tensorboard')

            # run new tensorboard process with new logging directory
            # tensorboard_process = Popen('tensorboard --logdir={:s}'.format(self.backup_dir), shell=True)
            # time.sleep(2)
            # webbrowser.open_new_tab('http://127.0.0.1:6006')

        print("Executing : ", training_command)

        train_process = Popen(training_command, shell=True, stderr=PIPE, preexec_fn=os.setsid)

        while train_process.poll() is None:

            out = train_process.stderr.readline().decode('utf-8')

            print(out)

            self.file_logger(out, os.path.dirname(self.training_dir),
                             'train_log.txt')
            sys.stdout.flush()

            if 'Saving weights to' in out and '-train_' in out and 'backup' not in out:
                msg = ('{:s} model was saved at step {:s}.\n'.format(
                    self.training_name,
                    msg.partition('-train_')[2].partition('.weights')[0]))
                self.file_logger(msg, os.path.join(HOME, 'slack'),
                                 'tmp.txt')

            if 'Finished training!' in out or ('Saving weights to' in out and '_final.weights' in out):
                time.sleep(5)
                return True

    # def export_model(self):

    #     exported_models_dir = os.path.join(self.base_path, 'Models', 'Exported')

    #     if self.mode == 'TF':
    #         # PROD
    #         # ! Automate cloning of models repo of tensorflow (https://github.com/tensorflow/models.git)

    #         cnums = []
    #         # find latest checkpoint
    #         for filename in glob.glob(self.backup_dir + '/*.meta'):
    #             cnum = int(
    #                 filename.partition('model.ckpt-')[2].partition('.meta')[0])
    #             cnums.append(cnum)
    #         # if checkpoint exists and is not at 0
    #         if (max(cnums) > 0 and cnums is not None):
    #             trained_checkpoint_prefix = os.path.join(
    #                 self.backup_dir, 'model.ckpt-' + str(max(cnums)))
    #             export_dir = os.path.join(
    #                 exported_models_dir,
    #                 self.rename_dir(exported_models_dir, self.training_name,
    #                                 '_v'))
    #             cmd = self.python_interpeter + ' {:s}/export_inference_graph.py --input_type=image_tensor --pipeline_self.config_path={:s} --trained_checkpoint_prefix={:s} --output_directory={:s}'.format(
    #                 self.object_detection_path, self.config_path,
    #                 trained_checkpoint_prefix, export_dir)
    #             exp_process = Popen(
    #                 cmd,
    #                 shell=True,
    #                 stdout=PIPE,
    #                 stderr=STDOUT,
    #                 universal_newlines=True,
    #                 preexec_fn=os.setsid)
    #             # log output while process is running
    #             self.file_logger('=======================',
    #                              os.path.dirname(self.training_dir),
    #                              'train_log.txt')
    #             self.file_logger('Exporting Model',
    #                              os.path.dirname(self.training_dir),
    #                              'train_log.txt')
    #             print(colored('Exporting model...', 'blue'))
    #             while exp_process.poll() is None:
    #                 out = exp_process.stdout.readline().decode('utf-8')
    #                 self.file_logger(out, os.path.dirname(self.training_dir),
    #                                  'train_log.txt')
    #                 sys.stdout.flush()
    #             # copy label map when done
    #             shutil.copyfile(
    #                 os.path.join(self.training_dir, 'labelmap.pbtxt'),
    #                 os.path.join(export_dir, 'labelmap.pbtxt'))
    #             print(
    #                 colored('Exported model to: {:s}'.format(export_dir),
    #                         'green'))

    #     elif self.mode == 'YOLO':

    #         # setup paths to training files
    #         weights_file = os.path.join(
    #             self.backup_dir, self.training_name + '-train_final.weights')
    #         cfg_file = self.config_path.replace('-train.cfg', '-test.cfg')
    #         data_file = os.path.join(
    #             os.path.dirname(self.config_path),
    #             self.training_name + '.data')
    #         names_file = os.path.join(
    #             os.path.dirname(self.config_path),
    #             self.training_name + '.names')
    #         # create export directory
    #         export_dir = os.path.join(
    #             exported_models_dir,
    #             self.rename_dir(exported_models_dir, self.training_name, '_v'))
    #         self.create_dir(export_dir)
    #         # setup paths to export files
    #         self.x_weights = os.path.join(export_dir,
    #                                       weights_file.split('/')[-1])
    #         self.x_cfg = os.path.join(export_dir, cfg_file.split('/')[-1])
    #         x_names = os.path.join(export_dir, names_file.split('/')[-1])
    #         self.x_data = os.path.join(export_dir, data_file.split('/')[-1])
    #         # copy files to export directory
    #         shutil.copyfile(weights_file, self.x_weights)
    #         shutil.copyfile(cfg_file, self.x_cfg)
    #         shutil.copyfile(names_file, x_names)
    #         # create names file in export directory
    #         with open(self.x_data, 'w') as f:
    #             f.write('classes = {:d}\nnames = {:s}'.format(
    #                 self.num_classes, x_names))

    #     time.sleep(3)
    #     return export_dir

    def file_logger(self, msg, path, name):

        f = open(os.path.join(path, name), 'a')
        msg = msg if msg.endswith('\n') else msg + '\n'
        f.write(msg)
        f.close()

    def training_logger(self, success, export_dir=None):

        log = {
            'name': self.training_name,
            'model': self.model_name,
            'dataset': self.dataset_name
        }
        log['status'] = 'completed' if success is True else 'failed'
        configuration = self.hyperparameters
        results = {'training_set': {}, 'test_set': {}}
        log['configuration'] = configuration
        log['weights_path'] = None if export_dir is None else export_dir
        log['results'] = results
        with open('training_log.json', 'w') as outfile:
            json.dump(log, outfile, indent=2)

        return log

    def kill_process(self, process):

        # find running processes with given name
        p = Popen(
            'lsof -n -i | grep "{:s}"'.format(process),
            shell=True,
            stdout=PIPE)
        # store output from shell process
        out, err = p.communicate()
        if out != b'':
            # extract process id from output
            pids = re.findall('(?<={:s} )[0-9]+'.format(process),
                              out.decode('utf-8'))
            for pid in pids:
                # send sigterm (ctrl-c) to kill process
                os.kill(int(pid), signal.SIGTERM)

    # def run_testing(self):

    #     images_train = os.path.join(self.dataset_dir, 'data/train.txt')
    #     images_test = os.path.join(self.dataset_dir, 'data/test.txt')
    #     label_dir = os.path.join(self.dataset_dir, 'labels')
    #     export_train_dir = os.path.join(self.base_path, 'Inferences',
    #                                     self.training_name, 'trainset')
    #     export_test_dir = os.path.join(self.base_path, 'Inferences',
    #                                    self.training_name, 'testset')
    #     iou_thresh = 0.5
    #     conf_thresh = 0.5

    #     print(colored('Running inference on test set...', 'blue'))
    #     if self.mode == 'YOLO':
    #         dn_save(images_train, export_test_dir, self.x_cfg, self.x_data,
    #                 self.x_weights)
    #     elif self.mode == 'TF':
    #         model = os.path.join(export_dir, 'frozen_inference_graph.pb')
    #         labelmap = os.path.join(export_dir, 'labelmap.pbtxt')
    #         tf_save(images_train, export_test_dir, labelmap, model)

    #     print(
    #         colored(
    #             'Detection images and labels saved in {:s}'.format(
    #                 export_test_dir), 'green'))
    #     iou_thresh = 0.5
    #     conf_thresh = 0.5
    #     image_dir = os.path.join(self.dataset_dir, 'images')
    #     label_dir = os.path.join(self.dataset_dir, 'labels')
    #     expfolder = os.path.join(export_test_dir, 'kpis')
    #     x, AP_values, mAP, Recall_per_image, overall_Recall, Precision_per_image, overall_Precision, F1_per_image, overall_F1 = calculateKPIs(
    #         self.mode, self.training_name, label_dir, export_test_dir,
    #         expfolder, image_dir, iou_thresh, conf_thresh)

    #     print(
    #         colored(
    #             "Mean Average Precision (mAP) for test set: {:f}".format(mAP),
    #             'green'))

    #     print(colored('Running inference on training set...', 'blue'))
    #     if self.mode == 'YOLO':
    #         dn_save(images_train, export_train_dir, self.x_cfg, self.x_data,
    #                 self.x_weights)
    #     elif self.mode == 'TF':
    #         tf_save(images_train, export_train_dir, labelmap, model)
    #     print(
    #         colored(
    #             'Detection images and labels saved in {:s}'.format(
    #                 export_train_dir), 'green'))

    #     x, AP_values, mAP, Recall_per_image, overall_Recall, Precision_per_image, overall_Precision, F1_per_image, overall_F1 = calculateKPIs(
    #         self.mode, self.training_name, label_dir, export_train_dir,
    #         expfolder, image_dir, iou_thresh, conf_thresh)
    #     print(
    #         colored(
    #             'Mean Average Precision (mAP) for train set: {:f}'.format(mAP),
    #             'green'))
    #     print(colored('Training and testing are complete!', 'green'))
