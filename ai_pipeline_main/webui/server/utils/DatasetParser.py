import os
import shutil
import random

import io
import hashlib
import random
import tensorflow as tf
from PIL import Image
import cv2 as cv

from app import app

# TODO: Replace pipeline_base_training_path with a globally accessible variable through app config


class DatasetParser:

    def __init__(self, training_name, dataset_path, pipeline_base_training_path, label_map_name='labelmap.pbtxt'):
        """Initialize dataset for training.

        Prerequisite knowledge: 
        There exists a base folder for all AI PIPELINE related operations. 
        Let this folder be AI_PIPELINE_BASE.
        All generated data files, checkpoints, backups will be inside this folder.
        This initializer class creates a new folder for a new model to be trained, and generates all the required files
        by scanning the chosen dataset folder and parsing its contents.

        Takes training name as an input, and does the following:
        - Creates training folder with this name inside AI_PIPELINE_BASE/Training/
        - Creates config folder inside previously created training folder
        - Creates backup directory for checkpointing during training, inside previously created training folder

        Arguments:
            training_name {str} -- Name of current training routine
            dataset_path {str} -- Selected training dataset path
            pipeline_base_training_path {str} -- Base path for pipeline data storage
        """

        # Argument consumption
        self.dataset_path = dataset_path
        self.pipeline_base_training_path = pipeline_base_training_path
        self.training_name = training_name
        self.label_map_name = label_map_name

        # Construct some helpful directories for use throughout the class
        self.images_directory = os.path.join(self.dataset_path, 'images')
        self.labels_directory = os.path.join(self.dataset_path, 'labels')

        self.data_directory_required_files = ['test.txt', 'train.txt', 'train.record', 'test.record']

        self.training_name = self.rename_with_version(self.training_name, self.pipeline_base_training_path, "_v")
        self.backup_name = self.rename_with_version(
            self.training_name + "_backup", self.pipeline_base_training_path, "_v")

        # BASE/training_name
        # BASE/training_name/config
        # BASE/training_name/backup
        self.training_directory = os.path.join(self.pipeline_base_training_path, self.training_name)
        self.training_config_directory = os.path.join(self.training_directory, "config")
        self.backup_directory = os.path.join(self.training_directory, self.backup_name)

        # TODO: Refactor to store inside TRAINING inside pipeline directory, separate from dataset
        self.data_directory = os.path.join(self.training_directory, 'data')

        self.names_file_path = os.path.join(self.dataset_path, 'classes.txt')

        self.label_map_path = os.path.join(self.training_directory, self.label_map_name)

    def initialize(self):

        self.create_training_directories()
        self.create_data_directories()
        self.check_names_file()
        self.create_label_map()
        self.create_train_test_records()
        self.copy_default_inputs()

    def copy_default_inputs(self):

        # Copy default inputs
        self.default_inputs_path = os.path.join(self.pipeline_base_training_path, 'default_inputs.json')

        if not os.path.exists(self.default_inputs_path):

            original_path = os.path.join(app.root_path, '..', 'utils', 'default_inputs.json')
            shutil.copyfile(original_path, self.default_inputs_path)

        return True

    def create_training_directories(self):

        # Create training directories /dataset_path/training_name, /dataset_path/training_name/config
        self.create_directories(self.training_directory, self.training_config_directory, self.backup_directory)

    def create_data_directories(self):

        # Create data directories inside DATASET directory

        # Check if directory exists, and if it does check if required files are present
        if os.path.exists(self.data_directory):
            data_directory_files = os.listdir(self.data_directory)

            # Check if all required files are present, otherwise delete data directory
            self.required_data_files_exist = True  # Save the check in a member variable for use elsewhere
            if not set(self.data_directory_required_files).issubset(set(data_directory_files)):
                shutil.rmtree(self.data_directory)
                self.required_data_files_exist = False

        # Whether it didnt exists from the beginning or it was just deleted, recreate data directory
        if not os.path.exists(self.data_directory):
            self.create_directories(self.data_directory)
            self.required_data_files_exist = False

    def check_names_file(self):

        # Check if names file already exists
        # By names file here, we mean classes.txt which has to be uploaded with every dataset

        if not "classes.txt" in os.listdir(self.dataset_path):

            print("Names file 'classes.txt' does not exist.")
            # Issue error
            # TODO: Refactor error message into an actual error
            raise ValueError(
                "classes.txt does not exist in current dataset. Please re-upload using dataset server webpage.")
            return False

        else:

            names_file = open(self.names_file_path)
            self.n_classes = len(names_file.read().strip().split('\n'))
            names_file.close()

            return True

    def create_label_map(self):

        self.class_names = []

        with open(self.names_file_path) as names_file_object:
            self.class_names = names_file_object.readlines()

        self.class_names = [c.strip() for c in self.class_names if len(c) > 0]

        with open(self.label_map_path, 'w+') as label_file_object:

            proto_string = "\nitem{{\n\tid: {}\n\tname: '{}' \n }}\n"

            for i, class_name in enumerate(self.class_names):
                label_file_object.write(proto_string.format(i + 1, class_name))

        return True

    def create_train_test_records(self):

        self.train_file_path = os.path.join(self.data_directory, 'train.txt')
        self.test_file_path = os.path.join(self.data_directory, 'test.txt')

        # If .record files dont exist
        if not self.required_data_files_exist:

            # Split dataset into train/test
            self.split_dataset(ratio=0.9)

            # Create .records files
            self.train_record_file_path = os.path.join(self.data_directory, 'train.record')
            self.test_record_file_path = os.path.join(self.data_directory, 'test.record')

            # TODO: Refactor this by creating a file parser module + all other usages: priority (200)
            train_labels_paths = [file_name.strip().replace('images', 'labels')[:-4] +
                                  '.txt' for file_name in self.train_files_paths]
            test_labels_paths = [file_name.strip().replace('images', 'labels')[:-4] +
                                 '.txt' for file_name in self.test_files_paths]

            self.n_training_files = len(train_labels_paths)
            self.n_testing_files = len(test_labels_paths)

            train_writer = tf.python_io.TFRecordWriter(self.train_record_file_path)
            test_writer = tf.python_io.TFRecordWriter(self.test_record_file_path)

            # Create a single line describing image and annotations
            for _id, (file_path, label_path) in enumerate(zip(self.train_files_paths, train_labels_paths)):

                line = self.get_tf_record(file_path, label_path)
                train_writer.write(line.SerializeToString())

            for _id, (file_path, label_path) in enumerate(zip(self.test_files_paths, test_labels_paths)):

                line = self.get_tf_record(file_path, label_path)
                test_writer.write(line.SerializeToString())

        else:

            with open(self.train_file_path, 'r') as file:

                lines = file.readlines()
                self.n_training_files = len(lines)

            with open(self.test_file_path, 'r') as file:

                lines = file.readlines()
                self.n_testing_files = len(lines)

        # Draw ground truth
        self.ground_truth_directory = os.path.join(self.data_directory, 'ground_truth')
        self.ground_truth_exists = os.path.exists(self.ground_truth_directory)

        # TODO: Implement ground truth drawing on new label format : priority (4)
        # if not self.ground_truth_exists or (self.ground_truth_exists and len(os.listdir(self.ground_truth_directory)) == 0):
        #     self.draw_ground_truths()
        #     self.ground_truth_exists = True

        return True

    def split_dataset(self, ratio=0.9):

        # Delete files if they aready exist
        if os.path.exists(self.train_file_path):
            os.remove(self.train_file_path)
        if os.path.exists(self.test_file_path):
            os.remove(self.test_file_path)

        file_list = [file for file in os.listdir(self.images_directory)
                     if file.endswith(tuple(['.png', '.jpg', '.jpeg']))]
        random.shuffle(file_list)

        boundary = int(round(len(file_list) * ratio))

        # Divide dataset files into training files and testing files
        train_files = file_list[:boundary]
        test_files = file_list[boundary:]

        # Create list of files paths
        self.train_files_paths = [os.path.join(self.images_directory, file) for file in train_files]
        self.test_files_paths = [os.path.join(self.images_directory, file) for file in test_files]

        # Write training files paths
        train_file_object = open(self.train_file_path, 'a+')  # Create file if not exist, and append to it

        for file_path in self.train_files_paths:
            train_file_object.write(file_path + '\n')
        train_file_object.close()

        # Write testing files paths
        test_file_object = open(self.test_file_path, 'a+')

        for file_path in self.test_files_paths:
            test_file_object.write(file_path + '\n')
        test_file_object.close()

        return True

    def draw_ground_truths(self):

        for image_path in (self.train_files_paths + self.test_files_paths):

            # Get image name
            image_name = os.path.basename(image_path)

            # Get corresponding label path
            label_path = os.path.join(self.labels_directory, os.path.splitext(image_name)[0] + '.txt')

            bounding_box = yolo2abs(image, label)

            draw_box(image, bbox, output_dir, names)

    def create_directories(self, *argv):

        # Loop over directories and create them
        for arg in argv:
            try:
                os.makedirs(arg)
            except OSError as e:
                raise ValueError("Error creating directories. {}".format(e))

    def rename_with_version(self, file_name, parent_directory_path, separator):
        """Rename file/directory by appending corresponding version to its name, depending on how many versions already
        exist.

        Arguments:
                file_name {str} -- [description]
                separator {str} -- [description]

        Returns:
                str -- Updated file name with correct version appended
        """
        isolated_name = file_name.split(separator)[0]
        version = 1
        while os.path.exists(os.path.join(parent_directory_path, file_name)):
            file_name = isolated_name + str(separator) + str(version)
            version = version + 1

        return file_name

    def int64_feature(self, value):
        return tf.train.Feature(int64_list=tf.train.Int64List(value=[value]))

    def int64_list_feature(self, value):
        return tf.train.Feature(int64_list=tf.train.Int64List(value=value))

    def bytes_feature(self, value):
        return tf.train.Feature(bytes_list=tf.train.BytesList(value=[value]))

    def bytes_list_feature(self, value):
        return tf.train.Feature(bytes_list=tf.train.BytesList(value=value))

    def float_list_feature(self, value):
        return tf.train.Feature(float_list=tf.train.FloatList(value=value))

    def get_tf_record(self, image_path, label_path):

        # load image
        with tf.gfile.GFile(image_path, 'rb') as fid:
            encoded_img = fid.read()

        encoded_img_io = io.BytesIO(encoded_img)
        image = Image.open(encoded_img_io)
        key = hashlib.sha256(encoded_img).hexdigest()

        width, height = image.size

        # read annotation
        with open(label_path, 'r') as li:
            annotations = li.readlines()

        # convert annotations to tensorflow format
        xmin = []
        ymin = []
        xmax = []
        ymax = []
        classes = []
        classes_text = []
        truncated = []
        poses = []
        difficult_obj = []

        for a in annotations:

            c_id, x, y, w, h = a.strip().split(' ')
            c_id = int(c_id)

            x = float(x)
            y = float(y)
            w = float(w)
            h = float(h)

            xmin.append(float(x - (w / 2)))
            ymin.append(float(y - (h / 2)))
            xmax.append(float(x + (w / 2)))
            ymax.append(float(y + (h / 2)))
            # class 0 is for background?
            classes.append(c_id + 1)
            classes_text.append(self.class_names[c_id].encode('utf8'))
            # ????????????????????????????????????
            truncated.append(0)
            poses.append(''.encode('utf8'))
            difficult_obj.append(int(False))

        # create tfrecords
        example = tf.train.Example(features=tf.train.Features(feature={
            'image/height': self.int64_feature(height),
            'image/width': self.int64_feature(width),
            'image/filename': self.bytes_feature(image_path.encode('utf8')),
            'image/source_id': self.bytes_feature(image_path.encode('utf8')),
            'image/key/sha256': self.bytes_feature(key.encode('utf8')),
            'image/encoded': self.bytes_feature(encoded_img),
            'image/format': self.bytes_feature(image_path[-3:].encode('utf8')),
            'image/object/bbox/xmin': self.float_list_feature(xmin),
            'image/object/bbox/xmax': self.float_list_feature(xmax),
            'image/object/bbox/ymin': self.float_list_feature(ymin),
            'image/object/bbox/ymax': self.float_list_feature(ymax),
            'image/object/class/text': self.bytes_list_feature(classes_text),
            'image/object/class/label': self.int64_list_feature(classes),
            'image/object/difficult': self.int64_list_feature(difficult_obj),
            'image/object/truncated': self.int64_list_feature(truncated),
            'image/object/view': self.bytes_list_feature(poses),
        }))

        return example

    def yolo2abs(self, image_path, label_path):
        """ Extracts bounding box from label and image

        Does this by converting the relative coordinates in the labels file into absolute coordinates using
        the image width and height.

        Arguments:
            image_path {str} 
            label_path {str} 

        Returns:
            Bounding box object
        """
        bbox = {}

        img = cv.imread(image_path)
        height, width = img.shape[0], img.shape[1]

        label_exists = os.path.isfile(label_path)
        label_empty = True if label_exists is not True else os.stat(label_path).st_size == 0

        if label_exists is False or label_empty is True:

            return bbox

        else:
            with open(label_path, 'r') as f:
                yolo = f.readlines()
            for line in yolo:
                line = line.rstrip('\n')
                line = line.strip().split()
                ctype = int(line[0])
                left = round((float(line[1]) - float(line[3]) / 2) * width)
                top = round((float(line[2]) - float(line[4]) / 2) * height)
                right = round((float(line[1]) + float(line[3]) / 2) * width)
                bottom = round((float(line[2]) + float(line[4]) / 2) * height)

                if len(line) == 6:
                    # bbox.append([ctype, left, top, right, bottom, round(float(box[5]))])

                    bbox['ctype'] = ctype
                    bbox['left'] = left
                    bbox['right'] = right
                    bbox['bottom'] = bottom
                    # TODO: What the hell is box[5] ?? Ask christophe. : priority (4)

                elif len(line) == 5:
                    # bbox.append([ctype, left, top, right, bottom])

                    bbox['ctype'] = ctype
                    bbox['left'] = left
                    bbox['right'] = right
                    bbox['bottom'] = bottom

                else:
                    print('Error in annotation format. Check file {:s}'.format(label_path))
                    exit()

            return bbox

    def draw_box(image_name, bbox, output_dir, names=None):

        colors = [(255, 0, 0), (0, 255, 0), (0, 0, 255), (125, 0, 255), (0, 125, 255),
                  (255, 125, 0), (0, 255, 125), (125, 255, 0), (255, 0, 125)]

        random.seed(0)
        random.shuffle(colors)

        img = cv.imread(image_name)

        for box in bbox:

            color = colors[box[0]] if box[0] < 8 else (0, 0, 0)
            img = cv.rectangle(img, (box[1], box[2]), (box[3], box[4]), color, 2)
            name = names[box[0]] if names is not None else str(box[0])
            top_txt = box[2] - 10
            img = cv.putText(img, name, (box[1], top_txt), cv.FONT_HERSHEY_SIMPLEX, 0.8, (255, 255, 255), 2)

        output_name = os.path.join(output_dir, image_name.split('/')[-1])

        cv.imwrite(output_name, img)

        print('Image saved to {:s}.'.format(output_name))
