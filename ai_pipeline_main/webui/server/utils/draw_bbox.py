import os, random
import numpy as np
import cv2 as cv

IMG_FORMAT = tuple(['.jpg', '.png', '.jpeg'])


def yolo2abs(image_name, label_name):
    print(image_name, label_name)
    bbox = []
    img = cv.imread(image_name)
    height, width = img.shape[0], img.shape[1]
    label_exists = os.path.isfile(label_name)
    label_empty = True if label_exists is not True else os.stat(
        label_name).st_size == 0
    if label_exists is False or label_empty is True:
        return bbox
    else:
        with open(label_name, 'r') as f:
            yolo = f.readlines()
        for line in yolo:
            line = line.rstrip('\n')
            line = line.strip().split()
            ctype = int(line[0])
            left = round((float(line[1]) - float(line[3]) / 2) * width)
            top = round((float(line[2]) - float(line[4]) / 2) * height)
            right = round((float(line[1]) + float(line[3]) / 2) * width)
            bottom = round((float(line[2]) + float(line[4]) / 2) * height)
            if len(line) == 6:
                bbox.append(
                    [ctype, left, top, right, bottom,
                     round(float(box[5]))])
            elif len(line) == 5:
                bbox.append([ctype, left, top, right, bottom])
            else:
                print('Error in annotation format. Check file {:s}'.format(
                    label_name))
                exit()
        print(bbox)
        return bbox


def draw_box(image_name, bbox, output_dir, names=None):

    colors = [(255, 0, 0), (0, 255, 0), (0, 0, 255), (125, 0, 255),
              (0, 125, 255), (255, 125, 0), (0, 255, 125), (125, 255, 0),
              (255, 0, 125)]
    random.seed(0)
    random.shuffle(colors)
    print(image_name)
    img = cv.imread(image_name)
    # #check if the image is being displayed correctly
    # cv.imshow('img', img)
    # cv.waitKey(0)
    # cv.destroyAllWindows()

    for box in bbox:
        color = colors[box[0]] if box[0] < 8 else (0, 0, 0)
        img = cv.rectangle(img, (box[1], box[2]), (box[3], box[4]), color, 2)
        name = names[box[0]] if names is not None else str(box[0])
        top_txt = box[2] - 10
        img = cv.putText(img, name, (box[1], top_txt), cv.FONT_HERSHEY_SIMPLEX,
                         0.8, (255, 255, 255), 2)

    # visualize an image with bounding boxes
    # cv.imshow('img2', img)
    # cv.waitKey(0)
    # cv.destroyAllWindows()

    #PROD
    # ! create all important directories in the beginning of the process !
    # check if a folder "ground_truth" exists
    if (os.path.isdir(output_dir) == False):
        os.mkdir(output_dir)
    else:
        pass

    output_name = os.path.join(output_dir,
                               image_name.split('/')[-1])[0:-3] + 'jpg'
    cv.imwrite(output_name, img)
    print('Image saved to {:s}.'.format(output_name))


def save(image_src, output_dir, names_file=None):

    if names_file is not None:
        with open(names_file, 'r') as f:
            r = f.readlines()
        names = []
        for line in r:
            line = line.rstrip('\n')
            line = line.strip()
            names.append(line)
    else:
        names = None

    image_list = []

    if os.path.exists(image_src):
        if os.path.isdir(image_src):
            image_list = [
                os.path.join(image_src, file) for file in os.listdir(image_src)
                if file.endswith(IMG_FORMAT)
            ]
        if image_src.endswith('.txt'):
            with open(image_src, 'r') as f:
                r = f.readlines()
                r = list(filter(('').__ne__, r))
            for item in r:
                image_list.append(item.rstrip('\n').strip())
        if image_src.endswith(IMG_FORMAT):
            image_list = [image_src]

    #!------------------------------------------------------------------------
    #PROD
    #Olim: once again the list of files is based on the images from directory.
    # I added a filtering by checking if the labels exist
    print(len(image_list))
    filtered_image_list = []
    for x in range(len(image_list)):
        label = image_list[x].strip().replace('images', 'labels')[:-4] + '.txt'
        if os.path.isfile(label):
            filtered_image_list.append(image_list[x])
    image_list = filtered_image_list
    print(len(image_list))
    #!------------------------------------------------------------------------

    for image in image_list:
        name = image.split('/')[-1]
        label_dir = os.path.join(
            (os.path.dirname(os.path.normpath(image.split(name)[0]))),
            'labels')
        label = os.path.join(label_dir, ''.join([name[0:-3], 'txt']))
        bbox = yolo2abs(image, label)
        draw_box(image, bbox, output_dir, names)


if __name__ == '__main__':

    image_src = '/home/olimjon/Deep_Learning/Datasets/placebot_detection_top1000/images/muc_caro_color_2018-10-26-12-12-20.jpg'
    output_dir = '/home/olimjon/Deep_Learning/Datasets/placebot_detection_top1000/ground_truth'

    save(image_src, output_dir)

    print('Done.')
