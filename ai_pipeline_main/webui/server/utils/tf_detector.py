import numpy as np
import os
import six.moves.urllib as urllib
import cv2 as cv
import sys
import tarfile
import tensorflow as tf
import zipfile
import argparse
from collections import defaultdict
from io import StringIO
from matplotlib import pyplot as plt
from PIL import Image

def load_graph(model, labelmap):

    detection_graph = tf.Graph()
    with detection_graph.as_default():
        od_graph_def = tf.GraphDef()
        with tf.gfile.GFile(model,'rb') as fid:
            serialized_graph = fid.read()
            od_graph_def.ParseFromString(serialized_graph)
            tf.import_graph_def(od_graph_def, name='')

    # label_map = label_map_util.load_labelmap(labelmap)
    # categories = label_map_util.convert_label_map_to_categories(label_map, max_num_classes=num_classes, use_display_name=True)
    # category_index = label_map_util.create_category_index(categories)

    return detection_graph

def load_image_into_numpy_array(image, width, height):
  return np.array(image.getdata()).reshape((width, height, 3)).astype(np.uint8)

def run_inference(detection_graph, image_list, output_dir):

    # create session and run inference
    with detection_graph.as_default():
        with tf.Session() as sess:
            # Get handles to input and output tensors
            ops = tf.get_default_graph().get_operations()
            all_tensor_names = {output.name for op in ops for output in op.outputs}
            tensor_dict = {}
            for key in ['num_detections','detection_boxes','detection_scores','detection_classes']:
                tensor_name = key + ':0'
                if tensor_name in all_tensor_names:
                  tensor_dict[key] = tf.get_default_graph().get_tensor_by_name(tensor_name)
            image_tensor = tf.get_default_graph().get_tensor_by_name('image_tensor:0')

            for image_path in image_list:

                # Loading and feeding image
                image = Image.open(image_path)
                (im_width, im_height) = image.size
                image_np = load_image_into_numpy_array(image, im_width, im_height)
                image_np_expanded = np.expand_dims(image_np, axis=0)
                output_dict = sess.run(tensor_dict,feed_dict={image_tensor: image_np_expanded})

                # all outputs are float32 numpy arrays, so convert types as appropriate
                output_dict['num_detections'] = int(output_dict['num_detections'][0])
                output_dict['detection_classes'] = output_dict['detection_classes'][0].astype(np.uint8)
                output_dict['detection_boxes'] = output_dict['detection_boxes'][0]
                output_dict['detection_scores'] = output_dict['detection_scores'][0]

                detection_classes = (np.reshape(output_dict['detection_classes'],(np.size(output_dict['detection_classes']),1))).astype(int)
                detection_boxes = np.copy(output_dict['detection_boxes'])
                detection_scores = (np.reshape(output_dict['detection_scores'],(np.size(output_dict['detection_scores']),1)))
                detections = np.concatenate((detection_classes,detection_boxes,detection_scores),axis=1)
                detections = np.zeros(np.shape(np.concatenate((detection_classes,detection_boxes,detection_scores),axis=1)))

                detections[:,0] = np.copy(detection_classes[:,0])
                detections[:,1] = np.copy(detection_boxes[:,1])
                detections[:,2] = np.copy(detection_boxes[:,0])
                detections[:,3] = np.copy(detection_boxes[:,3])
                detections[:,4] = np.copy(detection_boxes[:,2])
                detections[:,5] = np.copy(detection_scores[:,0])

                for i in range(np.shape(detections)[0]):
                  print(detections)
                  if detections[i,5] < 0.5:
                      detections[i,:] = 0
                detections = detections[~np.all(detections == 0, axis=1)]
                detections[:,0] = detections[:,0].astype(int) - 1
                detections[:,1] = np.round(detections[:,1]*im_width).astype(int)
                detections[:,2] = np.round(detections[:,2]*im_height).astype(int)
                detections[:,3] = np.round(detections[:,3]*im_width).astype(int)
                detections[:,4] = np.round(detections[:,4]*im_height).astype(int)

                np.savetxt(os.path.join(output_dir,(image_path.split('/')[-1][0:-3]+'txt')),detections,fmt='%d %d %d %d %d %1.2f')
                img = cv.imread(image_path)
                for row in detections:
                    img = cv.putText(img,str(int(row[0])),(int(row[1]),int(row[2]) - 10), cv.FONT_HERSHEY_SIMPLEX,0.5,(255,255,255),2,cv.LINE_AA)
                    img = cv.rectangle(img, (int(row[1]), int(row[2])), (int(row[3]),int(row[4])),(0,255,0),3)
                cv.imwrite(os.path.join(output_dir,(image_path.split('/')[-1])), img)

def tf_save(image_src,output_dir,labelmap,model):

    # create list of images to test
    if type(image_src) is list:
        image_list = image_src
    elif os.path.isdir(image_src):
        image_list = [os.path.join(image_src,file) for file in os.listdir(image_src) if file.endswith(tuple(['.png','.jpg','.jpeg']))]
    elif image_src.endswith('.txt'):
        f = open(image_src,'r')
        r = f.read()
        f.close()
        image_list = r.split('\n')
        try:
            image_list.remove('')
        except e:
            print(e)

    # Create output directory
    path = os.path.dirname(output_dir)
    name = output_dir.split('/')[-1]
    sep = '_v'
    output_name = rename_dir(path, name, sep)
    output_dir = os.path.join(os.path.dirname(output_dir),output_name)
    os.makedirs(output_dir)

    detection_graph = load_graph(model,labelmap)
    run_inference(detection_graph,image_list,output_dir)

def rename_dir(path,name,sep):
	# change the name of the directory if it exists
	name1 = name.split(sep)[0]
	i = 1
	while os.path.exists(os.path.join(path,name)):
		name = name1 + str(sep) + str(i)
		i = i + 1
	return name

if __name__ == '__main__':
    model = '/home/robotics/FYP/Models/Exported/faster_rcnn_50_aug1/frozen_inference_graph.pb'
    labelmap = '/home/robotics/FYP/Models/Exported/labelmap.pbtxt'
    image_src = '/home/robotics/FYP/Datasets/verb_right/verb_right_50/original/images'
    output_dir = '/home/robotics/FYP/Inferences/faster_rcnn_50_aug1/predictions/verb_right'

    tf_save(image_src,output_dir,labelmap,model)
    print('Inference Done!')
