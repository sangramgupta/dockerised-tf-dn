import os
import shutil
import urllib.request
import tarfile
from termcolor import colored


class CheckpointManager():

    def __init__(self, model_name, checkpoint_type, checkpoint_url,
                 checkpoint_name, mode, checkpoint_directory, object_detection_path):

        self.model_name = model_name
        self.checkpoint_type = checkpoint_type
        self.checkpoint_url = checkpoint_url
        self.checkpoint_name = checkpoint_name
        self.mode = mode
        self.checkpoint_directory = checkpoint_directory
        self.object_detection_path = object_detection_path

    def download_progress(self, count, blockSize, totalSize):

        progress = int(count * blockSize * 100 / totalSize)

        print('Download progress: {:d} %'.format(progress), end='\r')

    def get_default_checkpoint(self):

        if self.mode == 'YOLO':

            checkpoint_path = os.path.join(self.checkpoint_directory, self.model_name,
                                           self.checkpoint_name)

            checkpoint_folder = os.path.dirname(checkpoint_path)

            if not os.path.exists(checkpoint_path):

                if not os.path.exists(checkpoint_folder):
                    os.makedirs(checkpoint_folder)

                print(
                    colored(
                        'Checkpoint not found. Downloading pretrained model...',
                        'blue'))

                response = urllib.request.urlretrieve(self.checkpoint_url,
                                                      checkpoint_path,
                                                      reporthook=self.download_progress)

        elif self.mode == 'TF':

            # PROD
            # ! check what happens if some folders in the path do not exist
            # ! Avoid creating folders manually
            checkpoint_path = os.path.join(self.checkpoint_directory, self.checkpoint_name)

            download_path = os.path.join(self.checkpoint_directory,
                                         self.checkpoint_name + '.tar.gz')

            if os.path.exists(checkpoint_path) is False:

                os.makedirs(checkpoint_path)

                print(
                    colored(
                        'Checkpoint not found. Downloading pretrained model...',
                        'blue'))

                print(self.checkpoint_url)
                print(download_path)

                response = urllib.request.urlretrieve(
                    self.checkpoint_url,
                    download_path,
                    reporthook=self.download_progress)
                # extract archive and delete it
                tar = tarfile.open(download_path, "r:gz")
                tar.extractall(self.checkpoint_directory)
                tar.close
                os.remove(download_path)

        return checkpoint_path

    def setup(self):

        if self.checkpoint_type == 'default':

            print("DEFAULT CHECKPOINT")

            self.checkpoint_path = self.get_default_checkpoint()

        elif self.checkpoint_type == 'scratch':

            self.checkpoint_path = -1

        return self.checkpoint_path
