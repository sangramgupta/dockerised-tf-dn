import unittest
import os
import shutil

from utils.DatasetInitializer import DatasetInitializer


class TestDatasetInitializer(unittest.TestCase):

    """Testing DatasetInitializer class functionality.
    """
    DATASET_PATH = "/home/rami/Desktop"
    BASE_PATH = "/home/rami/Desktop/base/training"

    TEST_DIRECTORIES = [os.path.join(DATASET_PATH, "model_03_v0")]

    dataset_initializer = DatasetInitializer("model_03_v0", DATASET_PATH, BASE_PATH)

    dataset_initializer.create_training_directories()
    dataset_initializer.create_data_directories()
    dataset_initializer.check_names_file()

    def test_create_training_directories(self):

        self.assertTrue(os.path.exists(self.dataset_initializer.training_directory))
        self.assertTrue(os.path.exists(self.dataset_initializer.training_config_directory))
        self.assertTrue(os.path.exists(self.dataset_initializer.backup_directory))

    def test_create_data_directories(self):

        self.assertTrue(os.path.exists(self.dataset_initializer.data_directory))

    def test_create_names_file(self):

        self.assertTrue(self.dataset_initializer.check_names_file())

    def test_create_label_map(self):

        self.assertTrue(self.dataset_initializer.create_label_map())
        self.assertTrue(os.path.exists(self.dataset_initializer.label_map_path))

    def test_create_train_test_records(self):

        self.assertTrue(self.dataset_initializer.create_train_test_records())
        # self.assertTrue(os.path.exists(dataset_initializer.data_directory))

    def test_copy_default_inputs(self):
        self.assertTrue(self.dataset_initializer.copy_default_inputs())

    # def test_initialize(self):

    #     self.dataset_initializer.initialize()
    #     self.assertTrue(True)

    # NO LONGER VALID TEST - DOESNT MAKE SENSE TO MAKE FUNCTION STATIC FOR TESTING
    # def test_rename_with_version(self):
    #     """Check if rename_with_version performs as expected.

    #     Three files are present on the desktop: model_03_v0, model_03_v1, model_03_v2
    #     We expect the function to output model_03_v3, applying the correct version to the name
    #     after scanning the directory.
    #     """

    #     # Create test directories on desktop

    #     for directory in self.TEST_DIRECTORIES:

    #         try:
    #             os.mkdir(directory)
    #         except OSError as e:
    #             print("Directory creation failed: %s" % e)

    #     self.assertEqual(
    #         self.dataset_initializer.rename_with_version("model_03_v0", "/home/rami/Desktop", "_v"),
    #         "model_03_v3"
    #     )

    #     # Delete test directories
    #     for directory in self.TEST_DIRECTORIES:
    #         shutil.rmtree(directory)


if __name__ == '__main__':
    unittest.main()
