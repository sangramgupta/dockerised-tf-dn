from flask import Flask, request
from flask_restful import Resource, Api
from flask_cors import CORS
import os, shutil, sys
import random, json, time
from pymongo import MongoClient
from bson.objectid import ObjectId
import datetime
import pytz
import threading
from subprocess import Popen
from subprocess import PIPE
from termcolor import colored


from server import app

# TASK: Refactor application initialization into __init__.py, and start using blueprints : (2)
api = Api(app)

# TODO: GLOBALIZE : priority (999)
client = MongoClient('localhost', 27017)


# TODO: is this import necessary? Track throughout file :priority (2)
# ONLY OCCURENCE: program.upload dataset. Check its functionality
import program


# TODO: GLOBALIZE : priority (999)
# sys.path.insert(0, '/home/robotics/ai_pipeline/webui')
sys.path.insert(0, '/home/olimjon/MyGit/ai_pipeline_main/webui')

from training import run

from models import training_forms, Hyperparameters, Trainings





# Make some variables available in flask shell
@app.shell_context_processor
def make_shell_context():
    return {"Trainings": training_forms}



# TASK: Refactor into api bp route
class TrainingModels(Resource):
    def fetchModels(self):
        # model_list = [
        #   { 'id': 1, 'mode': 'YOLO', 'name': 'YOLO v2', 'checkpoint': 'N/A' },
        #   { 'id': 2, 'mode': 'YOLO', 'name': 'YOLO v3', 'checkpoint': 'N/A' },
        #   { 'id': 3, 'mode': 'TF', 'name': 'SSD Mobilenet v1', 'checkpoint': 'N/A' },
        #   { 'id': 4, 'mode': 'TF', 'name': 'FasterRCNN Resnet50', 'checkpoint': 'N/A' },
        # ]
        col = client.ai_pipeline.models


        # TASK: Refactor all pymongo usage into mongoengine models and usages (4)
        return list(
            col.find({}, {
                'name': 1,
                'mode': 1,
                '_id': 0,
                'checkpoint_url': 1,
                'checkpoint_name': 1
            }))

    def get(self):
        model_list = self.fetchModels()
        return model_list


class InferenceModels(Resource):
    def fetchModels(self):
        model_list = [{
            'id': 1,
            'name': 'splitbot_closeup'
        }, {
            'id': 2,
            'name': 'pickbot_combox'
        }]
        return model_list

    def get(self):
        model_list = self.fetchModels()
        return model_list


class DatasetImages(Resource):
    def fetchImages(self, dataset):

        # TODO: based on dataset name find instance in DB and all relevant info (class names, number of images, etc.)
        col = client.ai_pipeline.datasets
        dataset_info = col.find({'name': dataset})
        print(list(dataset_info))


        # TODO: GLOBALIZE : priority (999)
        # dataset_dir = os.path.join('/home/robotics/Deep_Learning/Datasets',dataset)
        dataset_dir = os.path.join(
            '/home/olimjon/MyGit/ai_pipeline_main/test-dataset', dataset)
        truth_dir = os.path.join(dataset_dir, 'ground_truth')
        image_dir = os.path.join(dataset_dir, 'images')

        if os.path.exists(truth_dir):
            image_og = [
                os.path.join(truth_dir, image)
                for image in os.listdir(truth_dir)
                if image.endswith(tuple(['.jpg', '.png', '.jpeg']))
            ]
        elif os.path.exists(image_dir):
            image_og = [
                os.path.join(image_dir, image)
                for image in os.listdir(image_dir)
                if image.endswith(tuple(['.jpg', '.png', '.jpeg']))
            ]
        else:
            image_og = ['assets/no-image.png']

        if len(image_og) > 20:
            random.seed(0)
            random.shuffle(image_og)
            image_og = image_og[0:20]

        # TODO: GLOBALIZE : priority (999)
        # assets = '/home/robotics/ai_pipeline/webui/ui/client/dist/TrainTool/assets/training-images'
        assets = '/home/olimjon/MyGit/ai_pipeline_main/webui/client/dist/TrainTool/assets/training-images'
        for file in os.listdir(assets):
            os.remove(os.path.join(assets, file))

        if image_og != ['assets/no-image.png']:
            for image in image_og:
                img = image.split('/')[-1]
                shutil.copyfile(image,
                                os.path.join(assets,
                                             image.split('/')[-1]))
            # TODO: GLOBALIZE : priority (999)
            image_list = [
                os.path.join('assets/training-images',
                             file.split('/')[-1]) for file in image_og
            ]
        else:
            image_list = image_og

        return image_list

    def get(self):
        dataset = request.args.get('dataset')
        image_list = self.fetchImages(dataset)
        return image_list


class AvailableDatasets(Resource):
    def fetchDatasets(self):

        # dataset_list = ['pickbot_combox', 'placebot_initial', 'splitbot_closeup_v2', 'splitbot_closeup_v3']

        col = client.ai_pipeline.datasets

        datasets = list(col.find({}, {'name': 1, '_id': 0}))
        dataset_list = []

        for x in range(len(datasets)):
            dataset_list.append(datasets[x]['name'])

        return dataset_list

    def get(self):
        dataset_list = self.fetchDatasets()
        return dataset_list


class AvailableCheckpoints(Resource):
    def fetchCheckpoints(self, model, mode):
        checkpoint_list = ['Custom_Checkpoint']
        return checkpoint_list

    def get(self):
        model = request.args.get('model')
        mode = request.args.get('mode')
        checkpoint_list = self.fetchCheckpoints(model, mode)
        return checkpoint_list



# TASK: Refactor this into a worker process, and setup redis message queue 
class TrainingThread(object):
    def __init__(self):

        self.col_train_forms = client.ai_pipeline.training_forms
        self.col_model = client.ai_pipeline.models
        self.col_train = client.ai_pipeline.trainings
        thread = threading.Thread(target=self.run, args=())
        thread.daemon = True
        thread.start()

    def run(self):

        # print("Reached 'run training'")
        # pass
        while True:
            if self.col_train_forms.count_documents({}) == 0:
                pass
            else:
                count = self.col_train_forms.find({
                    'status': 'queue'
                }).sort([('date', 1)]).limit(1).count()
                if count == 0:
                    pass
                else:
                    job = self.col_train_forms.find({
                        'status': 'queue'
                    }).sort([('date', 1)]).limit(1)[0]
                    self.col_train_forms.update_one({
                        '_id': job['_id']
                    }, {'$set': {
                        'status': 'progress'
                    }})
                    print(
                        colored(
                            'Starting training job: {:s}'.format(
                                job['training_name']), 'green'))

                    start_date = datetime.datetime.utcnow()
                    success, train_log = run(job)
                    end_date = datetime.datetime.utcnow()
                    
                    train_log['start_date'] = start_date
                    train_log['end_date'] = end_date
                    train_upload = self.col_train.insert_one(train_log)
                    
                    if success is True:
                        print('Training successful!')
                        self.col_train_forms.update_one({
                            '_id': job['_id']
                        }, {'$set': {
                            'status': 'completed'
                        }})
                        self.col_model.update_one({
                            'name': job['model_name']
                        }, {'$push': {
                            'trainings': train_upload.inserted_id
                        }})
                    else:
                        print('Training failed!')
                        self.col_train_forms.update_one({
                            '_id': job['_id']
                        }, {'$set': {
                            'status': 'failed'
                        }})
                    time.sleep(10)


class TrainingForm(Resource):

    def upload_config(self, form):
        form['status'] = "queue"
        form['date'] = datetime.datetime.utcnow()
        col = client.ai_pipeline.training_forms
        col.insert_one(form)

    def post(self):

        r = request.form

        hyperparameters = {
            'batch': r['batch'],
            'subdivisions': r['subdivisions'],
            'steps': r['steps'],
            'rates': r['rates'],
            'rate_mode': r['rateMode'],
            'step1': r['step1'],
            'rate1': r['rate1'],
            'step2': r['step2'],
            'rate2': r['rate2'],
            'step3': r['step3'],
            'rate3': r['rate3'],
            'decay_rate': r['decay'],
            'decay_step': r['dstep']
        }

        trainingForm = {
            'dataset_name': r['datasetName'],
            'training_name': r['trainingName'],
            'model_name': r['modelName'],
            'checkpoint_mode': r['checkpoint'],
            'hyperparameters': hyperparameters
        }


        # TODO: GLOBALIZE : priority (999)
        # with open('/home/robotics/ai_pipeline/training-job.json', 'w') as f:
        with open('/home/olimjon/MyGit/ai_pipeline_main/training-job.json',
                  'w') as f:
            json.dump((trainingForm), f, indent=2)

        self.upload_config(trainingForm)

        return 'Form Received'


class InferenceForm(Resource):
    def post(self):
        r = request.form

        inferenceForm = {
            'imageSource': r['imageSrc'],
            'datasetSet': r['datasetSet'],
            'numImages': r['numImages'],
            'dataset': r['dataset'],
            'databaseTags': r['databaseTags'],
            'runMode': r['runMode']
        }


        # TODO: GLOBALIZE : priority (999)
        # with open('/home/robotics/ai_pipeline/inference-job.json', 'w') as f:
        with open('/home/olimjon/MyGit/ai_pipeline_main/inference-job.json',
                  'w') as f:
            json.dump((inferenceForm), f, indent=2)
        return 'Form Received'


class InferenceUpload(Resource):
    def post(self):

        inf_path = '/home/robotics/inf'
        inf_path_images = os.path.join(inf_path, 'images')
        inf_path_labels = os.path.join(inf_path, 'labels')

        if os.path.exists(inf_path) is False:
            os.makedirs(inf_path_images)
            os.makedirs(inf_path_labels)

        f = (request.files)
        for key in f.keys():
            for value in f.getlist(key):
                if key == 'image':
                    value.save(os.path.join(inf_path_images, value.filename))
                if key == 'label':
                    value.save(os.path.join(inf_path_labels, value.filename))

        return 'Files Received'


class DatasetUpload(Resource):
    def post(self):


        # TODO: GLOBALIZE : priority (999)
        # inf_path = '/home/robotics/Nextcloud/Deep-Learning/Datasets/placebot_detection_top1000'
        inf_path = '/home/olimjon/Nextcloud/Deep-Learning/Datasets/placebot_detection_top1000'
        inf_path_images = os.path.join(inf_path, 'images')
        inf_path_labels = os.path.join(inf_path, 'json')

        if os.path.exists(inf_path) is False:
            os.makedirs(inf_path_images)
            os.makedirs(inf_path_labels)

        f = (request.files)
        for key in f.keys():
            for value in f.getlist(key):
                if key == 'image':
                    value.save(os.path.join(inf_path_images, value.filename))
                if key == 'label':
                    value.save(os.path.join(inf_path_labels, value.filename))


        # TODO: GLOBALIZE : priority (999)
        # local_pc_home = '/home/robotics'
        # db_pc_home = '/home/robotics'
        local_pc_home = '/home/olimjon'
        db_pc_home = '/home/olimjon'
        dataset = ['placebot_detection_top1000']
        x = -1  # if -1 all files will be copied
        label_extension = 'json'
        usecases = ['placebot']
        mydb = client.ai_pipeline
        program.upload_new_dataset(mydb, local_pc_home, db_pc_home, dataset, x,
                                   label_extension, usecases)

        return 'Files Received'


class HostName(Resource):
    def get(self):

        # import socket
        # name = socket.gethostname()
        p = Popen('cat /etc/hostname', shell=True, stdout=PIPE)
        out, err = p.communicate()
        name = out.decode('utf-8')
        return name


class ModelConfigurations(Resource):
    def get(self):

        model = request.args.get('model')
        
        models_col = client.ai_pipeline.models
        trainings_col = client.ai_pipeline.trainings
        configurations = []
        # get model default configuration
        default = models_col.find_one({
            'name': model
        }, {
            'configuration': 1,
            '_id': 0
        })

        default['configuration']['name'] = 'default'
        default['configuration']['viewName'] = 'Default Configuration'
        default['configuration']['viewNameCheckpoint'] = 'Default Checkpoint'
        configurations.append(default['configuration'])
        # get custom configurations
        custom_trainings = list(
            models_col.find({
                'name': model
            }, {
                'trainings': 1,
                '_id': 0
            }))[0]
        if len(custom_trainings['trainings']) > 0:
            for training_id in custom_trainings['trainings']:
                print('Found training with ID: ', training_id)
                training = trainings_col.find_one({
                    '_id': ObjectId(training_id)
                }, {
                    'name': 1,
                    'configuration': 1,
                    '_id': 0
                })
                training['configuration']['name'] = training['name']
                view_name = training['name'].replace('_', ' ')
                training['configuration']['name'] = training['name']
                training['configuration']['viewName'] = view_name
                training['configuration']['viewNameCheckpoint'] = view_name
                configurations.append(training['configuration'])
        return configurations


class DatasetInformation(Resource):
    def get_info(self, col):

        info = []
        result = list(col.find({}, {'_id': 0, 'name': 1}))
        for item in result:
            info.append(item['name'])
        return info

    def insert_info(self, item, col):
        result = col.insert_one({'name': item})

    def get(self):

        type = request.args.get('class')
        plant = request.args.get('plant')
        usecase = request.args.get('usecase')
        if type is not None:
            self.insert_info(type, client.ai_pipeline.classes)
            return 'added class'
        if plant is not None:
            self.insert_info(plant, client.ai_pipeline.plants)
            return 'added plant'
        if usecase is not None:
            self.insert_info(usecase, client.ai_pipeline.usecases)
            return 'added usecases'
        if all(v is None for v in [type, plant, usecase]):
            classes = self.get_info(client.ai_pipeline.classes)
            plants = self.get_info(client.ai_pipeline.plants)
            usecases = self.get_info(client.ai_pipeline.usecases)
            dataset_info = {
                'classes': classes,
                'plants': plants,
                'usecases': usecases
            }
            return dataset_info


api.add_resource(TrainingModels, '/training-models')
api.add_resource(InferenceModels, '/inference-models')
api.add_resource(DatasetImages, '/dataset-images')
api.add_resource(AvailableDatasets, '/available-datasets')
api.add_resource(AvailableCheckpoints, '/available-checkpoints')
api.add_resource(TrainingForm, '/training-form')
api.add_resource(InferenceForm, '/inference-form')
api.add_resource(InferenceUpload, '/inference-upload')
api.add_resource(HostName, '/pc-name')
api.add_resource(ModelConfigurations, '/model-configurations')
api.add_resource(DatasetInformation, '/dataset-information')
api.add_resource(DatasetUpload, '/dataset-upload')

cors = CORS(
    app,
    resources={
        r'/training-models': {
            'origins': '*'
        },
        r'/inference-models': {
            'origins': '*'
        },
        r'/dataset-images': {
            'origins': '*'
        },
        r'/available-datasets': {
            'origins': '*'
        },
        r'/available-checkpoints': {
            'origins': '*'
        },
        r'/training-form': {
            'origins': '*'
        },
        r'/inference-form': {
            'origins': '*'
        },
        r'/inference-upload': {
            'origins': '*'
        },
        r'/pc-name': {
            'origins': '*'
        },
        r'/model-configurations': {
            'origins': '*'
        },
        r'/dataset-information': {
            'origins': '*'
        },
        r'/dataset-upload': {
            'origins': '*'
        }
    })

if __name__ == "__main__":

    training = TrainingThread()
    app.run(host="0.0.0.0", port=4201)
