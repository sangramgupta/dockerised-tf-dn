import mongoengine
import MySQLdb
from pymongo import MongoClient

def mongo_connection(connection):
    # mongoengine.register_connection(alias=connection['alias'], name=connection['name'], db=connection['db'], host=connection['host'], port=connection['port'])
    client = MongoClient('10.180.129.185', 27017)
    mydb = client.ai_pipeline

    return mydb


def mysql_connection(conn_details):
    connection = MySQLdb.connect(host=conn_details['host'],user=conn_details['user'], passwd=conn_details['password'], db=conn_details['database'])
    return connection
