import os
import redis


BASE_DIR = os.path.abspath(os.path.dirname(__file__))


# Base configuration class with default variable values
class BaseConfig(object):

    SECRET_KEY = os.environ.get('SECRET_KEY') or '1ih298e1kabds9182yekhalkcn,fnlqj09wee190ejjslndadljqoue0912ueo'

    DEBUG = False
    TESTING = False
    CSRF_ENABLED = True

    TRAINING_QUEUE = "pipeline-training-queue"

    REDIS_URL = os.getenv('REDISTOGO_URL', 'redis://localhost:6379')
    REDIS_CONNECTION = redis.from_url(REDIS_URL)


class DevelopmentConfig(BaseConfig):

    DEBUG = True

    # Make sure the following directory exists
    # Flask will NOT create the directory for you

    MONGODB_HOST = "localhost"
    MONGODB_NAME = "ai_pipeline"
    MONGODB_PORT = 27017

    DATASET_FOLDER = "/home/rami/Desktop"
    AI_PIPELINE_BASE_FOLDER = "/home/rami/Desktop/base"


# Use this when deploying
class ProductionConfig(BaseConfig):

    DEBUG = False

    # Do not refactor into base config,
    # it isn't semantically correct to do so even though shorter code
    MONGODB_HOST = "10.180.142.120"
    MONGODB_NAME = "ai_pipeline"
    MONGODB_PORT = 27018

    DATASET_FOLDER = "/home/robotics/Nextcloud/Deep-Learning/Datasets"

# Use this when testing on network


class TestingConfig(BaseConfig):

    DEBUG = True
    TESTING = True

    MONGODB_HOST = "10.180.142.120"
    MONGODB_NAME = "ai_pipeline"
    MONGODB_PORT = 27018

    DATASET_FOLDER = "/home/rami/Desktop"
    AI_PIPELINE_BASE_FOLDER = "/home/rami/Desktop/base"
