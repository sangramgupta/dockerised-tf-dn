import os
from app import app
from rq import Worker, Queue, Connection

listen = [app.config["TRAINING_QUEUE"]]


if __name__ == '__main__':

    with Connection(app.config["REDIS_CONNECTION"]):
        worker = Worker(map(Queue, listen))
        worker.work()
