# from typing import List

# from data.trainings import Training
# from data.images import Image
# from data.plants import Plant
# from data.datasets import Dataset
# from data.models import Model
# from data.classes import ObjectClass
# from data.usecases import Usecase
# from data.annotations import GroundTruth


# ! ########## QUERIES ############

# def create_training(name: str) -> Training:
#     training = Training()
#     training.name = name
#
#     training.save()
#
#     return training
#
#
# def find_plant_by_name(name: str) -> Plant:
#     plant = Plant.objects(name=name).first()
#     return plant
#
#
# def find_usecase_by_name(name: str) -> Usecase:
#     usecase = Usecase.objects(name=name).first()
#     return usecase
#
#
# def find_usecases(names: list) -> List[Usecase]:
#     usecases = Usecase.objects(name__in=names).all()
#     return list(usecases)
#
#
# def find_dataset_by_name(name: str) -> Dataset:
#     dataset = Dataset.objects(name=name).first()
#     return dataset
#
#
#
# def find_model_by_name(name: str) -> Model:
#     model = Model.objects(name=name).first()
#     return model
#
# #
#
# def upload_new_image(filename, extension, path, width, height, plant, datasets, usecases, groundtruth, detections=None) -> Image:
#
#     image = Image()
#     image.filename = filename
#     image.extension = extension
#     image.path = path
#     image.width = width
#     image.height = height
#
#     plant = Plant.objects(name=plant).first()
#     image.plant_id = plant.id
#
#     datasets = Dataset.objects(name__in=datasets).all()
#     for dataset in datasets:
#         image.dataset_ids.append(dataset.id)
#
#     usecases = Usecase.objects(name__in=usecases).all()
#     for usecase in usecases:
#         image.usecase_ids.append(usecase.id)
#
#     image.ground_truth = groundtruth
#     if not detections:
#         image.detections.append(detections)
#     image.save()
#
#     return image
#
#
# def add_new_class(class_name) -> ObjectClass:
#     new_class = ObjectClass()
#     new_class.class_name = class_name
#
#     new_class.save()
#
#     return new_class
#
#
# def find_class_by_name(class_name) -> ObjectClass:
#     object_class = ObjectClass.objects(class_name=class_name).first()
#     return object_class
#
#
# def find_images_by_plant(plant_name) -> List[Image]:
#     images = []
#     plant = find_plant_by_name(plant_name)
#     if plant:
#         images = Image.objects().filter(plant_id=plant.id)
#     else:
#         print('There are no images from plant {}'.format(plant_name))
#     return images
#
#
# def create_new_usecase(usecase_name) -> Usecase:
#     usecase = Usecase()
#     usecase.name = usecase_name
#     usecase.save()
#     return usecase
#
#
# # ! OPTIMIZATION NEEDED: see "GET SNAKES FOR USER" in MONGODB-TUTORIAL
# def find_images_by_usecase(usecase_name) -> List[Image]:
#     images = []
#     usecase = Usecase.objects(name=usecase_name).first()
#
#     # usecase = find_usecase_by_name(usecase_name)
#     if usecase:
#         images = Image.objects(usecase__ids=usecase.id).all()
#
#     print(images)
#     return images
#
# # !
# def assign_dataset_to_image():
#     return None
#
# def create_new_dataset(name, plants, usecases, tags) -> Dataset:
#     image_ids = []
#
#     plant_ids = []
#     if len(plants)>0:
#         for plant in plants:
#             images_by_plant = find_images_by_plant(plant)
#         for image in images_by_plant:
#             print(image.plant_id)
#             if image.plant_id not in plant_ids:
#                 plant_ids.append(image.plant_id)
#             if image.id not in image_ids:
#                 image_ids.append(image.id)
#     print(plant_ids)
#
#     usecase_ids = []
#     # if len(usecases)>0:
#     #     for usecase in plants:
#     #         images_by_usecase = find_images_by_usecase(usecase)
#     #     for image in images_by_usecase:
#     #         print(image.usecase_ids)
#     #         if image.usecase_ids not in plant_ids:
#     #             plant_ids.append(image.plant_id)
#     #         if image.id not in image_ids:
#     #             image_ids.append(image.id)
#     print(plant_ids)
#
#
#     # no_of_images = len(images_by_plant)
#     # print(no_of_images)
#     # # ! TODO
#     # assign_dataset_to_image()
#
#
#     # new_dataset = Dataset()
#     # new_dataset.name = name
#     # new_dataset.number_of_images = no_of_images
#     # new_dataset.plant_ids = plant_ids
#     # new_dataset.usecase_ids = usecase_ids
#     # new_dataset.image_ids = image_ids
#
#     # new_dataset.save()
#
#     # return new_dataset
