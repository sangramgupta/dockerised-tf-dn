from flask import Flask
from flask import render_template
from config import *

import mongoengine
import os

app = Flask(__name__)

# Config object defined in a file on its own - separation of concerns
app.config.from_object(os.environ['APP_SETTINGS'])


from app import routes

# Import all models for availability for quick debugging in shell context
from app.models import *


# Register all required blueprints
from app.api import bp as api_blueprint
app.register_blueprint(api_blueprint, url_prefix="/apiv1")


# Connect to mongodb upon app initialization ONLY
mongoengine.connect(app.config['MONGODB_NAME'], host=app.config['MONGODB_HOST'], port=app.config['MONGODB_PORT'])


# Make some variables available in flask shell
@app.shell_context_processor
def make_shell_context():
    return {"training_forms": training_forms, "Models": Models,
            "Images": Images, "Datasets": Datasets, "Usecases": Usecases,
            "Plants": Plants}
