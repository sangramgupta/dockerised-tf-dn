import mongoengine



class GroundTruthModel(mongoengine.EmbeddedDocument):

	# Absolute values are the relative values in the labels files scaled up according to image height and width
	abs_x_min = mongoengine.IntField()
	abs_y_min = mongoengine.IntField()
	object_class_id = mongoengine.StringField()
	abs_height = mongoengine.IntField()
	abs_width = mongoengine.IntField()

	def __repr__(self):
		return '< Ground truth for {} >'.format(self.object_class_id);


class Images(mongoengine.Document):

	# Object ID automatically provided

	# Image width and height fetched from image metadata
	height = mongoengine.IntField()
	width = mongoengine.IntField()

	# Each image is taken in a single plant
	plant_id = mongoengine.ObjectIdField()

	# Dataset and usecase IDs
	usecase_ids = mongoengine.ListField(mongoengine.ObjectIdField())

	# Detections array: saves results from inferences, used later to calculate accuracy of models.
	# detections = mongoengine.EmbeddedDocumentListField(InferenceModel)

	# File metadata and OS information
	filename = mongoengine.StringField(unique=True)
	path = mongoengine.StringField(unique=True)
	extension = mongoengine.StringField()

	# Ground truth array
	ground_truth = mongoengine.EmbeddedDocumentListField(GroundTruthModel)

	def __repr__(self):
		
		return '< Image {} >'.format(self.filename)



class Usecases(mongoengine.Document):

	name = mongoengine.StringField();

	def __repr__(self):

		return '< Usecase {} >'.format(self.name)


class Plants(mongoengine.Document):

	name = mongoengine.StringField();

	def __repr__(self):
		
		return '< Plant {} >'.format(self.name);

class Datasets(mongoengine.Document):

	name = mongoengine.StringField(unique=True);
	
	n_images = mongoengine.IntField();

	plant_ids = mongoengine.ListField(mongoengine.ObjectIdField())
	usecase_ids = mongoengine.ListField(mongoengine.ObjectIdField())
	image_ids = mongoengine.ListField(mongoengine.ObjectIdField())


	def __repr__(self):
		
		return '< Dataset {} >'.format(self.name)


# TODO: Remove dynamic document, and change all mongo entries to remove trainings : priority (3)
class Models(mongoengine.DynamicDocument):

	name = mongoengine.StringField()
	mode = mongoengine.StringField()

	model = mongoengine.DynamicField()

	train_config = mongoengine.DynamicField()

	configuration = mongoengine.DynamicField()

	checkpoint_url = mongoengine.StringField()
	checkpoint_name = mongoengine.StringField()

	def __repr__(self):
		return '< Model {} object>'.format(self.name);


class Hyperparameters(mongoengine.EmbeddedDocument):

    decay_step = mongoengine.StringField()
    subdivisions = mongoengine.StringField()
    step1 = mongoengine.StringField()
    step2 = mongoengine.StringField()
    step3 = mongoengine.StringField()
    steps = mongoengine.StringField()

    decay_rate = mongoengine.StringField()
    rate1 = mongoengine.StringField()
    rate2 = mongoengine.StringField()
    rate3 = mongoengine.StringField()
    rates = mongoengine.StringField()
    rate_mode = mongoengine.StringField()

    batch = mongoengine.StringField()

    def __repr__(self):
        return '< Hyperparameters object {} >'.format(self.pk)


class training_forms(mongoengine.Document):

	training_name = mongoengine.StringField();
	status = mongoengine.StringField();
	date = mongoengine.DateTimeField();
	dataset_name = mongoengine.StringField();
	hyperparameters = mongoengine.EmbeddedDocumentField(Hyperparameters)

	model_name = mongoengine.StringField()
	checkpoint_mode = mongoengine.StringField()

	def __repr__(self):
		return '< Training form object: {} >'.format(self.training_name)


