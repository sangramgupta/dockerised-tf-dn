from flask import Blueprint


bp = Blueprint('api', __name__)


# Import must come after to prevent circular import of BP
# Seems dirty coming from cpp but is very normal in python due to the nature of imports here
from app.api import routes


