from app.api import bp
from app.models import *
from app.api.cors_utils import crossdomain
from app import app

from flask import request, jsonify
from redis import Redis
import rq


@bp.route('/models', methods=['GET'])
@crossdomain(origin="*")
def get_training_models():
    """Return list of all models on mongodb 

    Decorators:
            bp.route
            crossdomain

    Returns:
            Model [] - List of model objects
    """

    model_objects = Models.objects().only('name', 'mode', 'checkpoint_url', 'checkpoint_name')
    return model_objects.to_json()


@bp.route('/datasets', methods=['GET'])
@crossdomain(origin="*")
def get_available_datasets():
    """Return a list of all datasets on mongodb

    Decorators:
            bp.route
            crossdomain

    Returns:
            Dataset []  - List of dataset objects
    """

    dataset_objects = Datasets.objects()
    return dataset_objects.to_json()


@bp.route('/checkpoints', methods=["GET"])
@crossdomain(origin="*")
def get_available_checkpoints():
    """Endpoint for retrieving checkpoints.

    Accepts two query parameters as arguments, model<string> and mode<int>

    Decorators:
            bp.route

    Returns:
            Checkpoint [] - List of checkpoints
    """
    model = request.args.get('model')
    mode = request.args.get('mode')

    # TODO: Change this hardcoded checkpoint list to something fetched from database : priority (3)
    # TODO: Create checkpoint model in mongoengine and add to app/models.py
    checkpoint_list = ['Custom Checkpoint']

    return checkpoint_list


@bp.route('/configurations', methods=["GET"])
@crossdomain(origin="*")
def get_model_configurations():
    """ Returns list of model specific configurations

    Accepts one query parameters: model<string>

    Returns:
            Configuration []
    """

    model = request.args.get('model')

    # Get model default configuration from Models object

    # TODO: Error handling for fetching DB object: Make into a decorator since used so much : priority (1)
    model_object = Models.objects.get(name=model)
    default_configuration = model_object["configuration"]

    # TODO: Why does this have a name and view name? Check usages in angular client : priority (3)
    default_configuration["name"] = 'default'
    default_configuration["viewName"] = 'Default Configuration'
    default_configuration["viewNameCheckpoint"] = 'Default Checkpoint'

    # TODO: Figure out what to do with other configurations
    return jsonify([default_configuration])


@bp.route('/trainingjob', methods=["POST"])
@crossdomain(origin="*")
def submit_training_job():
    """ Retrieve all form contents and create training job 
    on redis queue server from them.

    Decorators:
            bp.route
            crossdomain

    Return:
            Boolean - Job enqueing success or failure.
    """

    hyperparameters = {

        'batch': request.form['batch'],
        'subdivisions': request.form['subdivisions'],
        'steps': request.form['steps'],
        'rates': request.form['rates'],
        'rate_mode': request.form['rateMode'],
        'step1': request.form['step1'],
        'rate1': request.form['rate1'],
        'step2': request.form['step2'],
        'rate2': request.form['rate2'],
        'step3': request.form['step3'],
        'rate3': request.form['rate3'],
        'decay_rate': request.form['decay'],
        'decay_step': request.form['dstep']
    }

    training_form = {

        'dataset_name': request.form['datasetName'],
        'training_name': request.form['trainingName'],
        'model_name': request.form['modelName'],
        'checkpoint_mode': request.form['checkpoint'],
        'hyperparameters': hyperparameters
    }

    # Submit job to redis queue, as training form object.
    queue = rq.Queue(app.config["TRAINING_QUEUE"], connection=Redis.from_url('redis://'))

    # Thousands of days timeout should be enough
    job = queue.enqueue('training.run', training_form, job_timeout=922337203)

    # Return success or failure
    # if job.get_id():
    # 	return jsonify({"status": "success"});
    # else:
    # 	return jsonify({"status": "success"});

    return ""
