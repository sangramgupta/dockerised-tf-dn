import setup
import queries.mongodb as mongo_queries
import queries.mysql as mysql_queries
import os, pwd
import json
import shutil
from pprint import pprint
from pymongo import errors as mongoerrors



# ! REMOTE MONGO DATABASE
server_mongo = {
    'alias':'core',
    'db':'ai_pipeline',
    'name':'ai_pipeline',
    'host':'10.180.129.185',
    'port':27017
}

# ! LOCAL MONGO DATABASE
local_mongo = {
    'alias':'core',
    'name':'ai_pipeline',
    'db':'ai_pipeline',
    'host':'localhost',
    'port':27017
}

# ! REMOTE MYSQL DATABASE
server_mysql = {
    'host':'10.180.129.185',
    'user':'root',
    'password':'robotics',
    'database':'labelingtool'
}

# ! LOCAL MYSQL DATABASE
server_mysql = {
    'host':'localhost',
    'user':'root',
    'password':'robotics',
    'database':'labelingtool'
}

# mydb = setup.mongo_connection(local_mongo) # connection to the database



# ! ########## TEST TICKETS HERE ############
def main():
# setup.mongo_connection(local_mongo) # connection to the database

    # * ADD NEW CLASSES
    # new_classes = ['container', 'assemply_shelf', 'train_shelf', 'dolly', 'wheel', 'cagebox', 'pallet', 'gripping_area']
    # if len(new_classes) > 0:
    #     for new_class in new_classes:
    #         add_new_class(new_class)
    # * FIND CLASS BY NAME
    # class_name = 'container'
    # find_class_by_name(class_name)
    # * ADD NEW IMAGES
    # name = "another new image"
    # ext = "jpeg"
    # path = "home"
    # width = 600
    # height = 600
    # plant = "munich"
    # datasets = ["dataset-2018"]
    # usecases = ["splitbot", "placebot"]
    # upload_new_image(name, ext, path, width, height, plant, list(datasets), list(usecases))
    # ! upload from local folder
    local_pc_home = pwd.getpwuid(os.getuid()).pw_dir
    db_pc_home = '/home/robotics'
    dataset = ['placebot_detection_top1000']
    x = -1 # if -1 all files will be copied
    label_extension = 'json'
    usecases = ['placebot']
    upload_new_dataset(mydb, local_pc_home, db_pc_home, dataset, x, label_extension, usecases)
    # * FIND IMAGES BY PLANT
    # plant_name = 'munich'
    # find_images_by_plant(plant_name)
    # * CREATE A NEW TRAINING
    # create_training()
    # * FIND MODEL BY NAME
    # * default model is yolo_default
    # model_name = 'yolo_default'
    # find_model_by_name()
    # * FIND PLANT BY NAME
#     plant_name = 'munich'
#     find_plant_by_name(plant_name)
    # * CREATE A NEW USECASE
    # usecase_names = ['splitbot', 'placebot', 'sortbot', 'pickbot']
    # for usecase_name in usecase_names:
    #     create_new_usecase(usecase_name)
    # ! CREATE NEW DATASET
#     dataset_name = 'new-dataset-2018'
#     plants = ['munich']
#     usecases = ['splitbot', 'placebot']
#     tags = []
#     create_new_dataset(dataset_name, plants, usecases, tags)
    # ! check for duplicate before upload
    # check_if_exists('images','filename', 'qqqqqqqqqqqqq')

    print("Main is running")




# ! ########## TICKETS ############

# CREATE A TRAINING
def create_training():

    # ! Dummy values

    name = "Faster RCNN"
    mongo_queries.create_training(name)

# UPLOAD NEW IMAGES
def upload_new_image(name, ext, path, width, height, plant, datasets, usecases, groundtruth, detections):
    mongo_queries.upload_new_image(name, ext, path, width, height, plant, datasets, usecases, groundtruth, detections)


# RETRIEVE DEFAULT MODEL SETTINGS
def find_model_by_name(model_name):
    model = mongo_queries.find_model_by_name(model_name)

    if model:
        print(model.checkpoint_name)


def find_plant_by_name(plant_name):
    plant = mongo_queries.find_plant_by_name(plant_name)
    print(type(plant))
    if plant:
        print(plant.name, plant.id)
    else:
        print("No plant was found")


def add_new_class(new_class):
    mongo_queries.add_new_class(new_class)


def find_class_by_name(class_name):
    object_class = mongo_queries.find_class_by_name(class_name)
    if object_class:
        print(object_class.id)
    else:
        print("No class was found")


def find_images_by_plant(plant_name):
    images = mongo_queries.find_images_by_plant(plant_name)
    for image in images:
        print(image.filename, image.id)


def create_new_usecase(usecase_name):
    mongo_queries.create_new_usecase(usecase_name)


def find_images_by_usecase(usecase_name):
    images = mongo_queries.find_images_by_usecase(usecase_name)
    for image in images:
        print(image.filename, image.id)


def create_new_dataset(dataset_name, plants, usecases, tags):
    mongo_queries.create_new_dataset(dataset_name, plants, usecases, tags)

# ! for leo's dataset
def plant_from_image_name(image_name):
    plant = image_name.split('_')[0]
    if plant == 'muc':
        return 'munich'
    elif plant == 'rburg':
        return 'regensburg'


def upload_new_dataset(mydb, local_pc_home, db_pc_home, dataset, x, label_ext, usecases):

    dataset_local_dir = os.path.join(local_pc_home, "Nextcloud/Deep-Learning/Datasets", dataset[0])
    database_local_dir = os.path.join(local_pc_home, "Nextcloud/Deep-Learning/Database")
    database_remote_dir = os.path.join(db_pc_home, "Nextcloud/Deep-Learning/Database")

    images_dataset_local = os.path.join(dataset_local_dir, 'images')
    images_database_local = os.path.join(database_local_dir, 'images')
    images_database_remote = os.path.join(database_remote_dir, 'images')

    labels_dataset_local = os.path.join(dataset_local_dir, label_ext)
    labels_database_local = os.path.join(database_local_dir, 'labels')
    labels_database_remote = os.path.join(database_remote_dir, 'labels')

    image_list = sorted(os.listdir(images_dataset_local))
    classes_file = os.path.join(dataset_local_dir, 'classes.txt')

    with open(classes_file, 'r') as f:
        classes = [x.replace('\n', '').lower() for x in f.read().splitlines(True)]
        print(classes)
    f.close()

    for i, image in enumerate(image_list):
        if i == x:
            break
        else:
            image_name, image_ext = os.path.splitext(image)
            image_ext = image_ext.replace('.', '')
            label = image_name + '.' + label_ext

            print(i, image_name, image_ext, label)

            label_path = os.path.join(labels_dataset_local, label)
            image_path = os.path.join(images_dataset_local, image)

            if (os.path.isfile(label_path)):
                # read json file
                data = json.load(open(label_path))
                # retrieve data
                width = int(data['image'][0]['width'])
                height = int(data['image'][0]['height'])
                remote_path = os.path.join(images_database_remote, image)
                local_path = os.path.join(images_database_local, image)

                plant = plant_from_image_name(image_name) # ! USe only  for leo's dataset

                labels = data['objects']

                groundtruth = []
                if len(labels) != 0:
                    for label in labels:
                        # pprint(label)
                        print(label['type'])
                        gt_label = {
                            'abs_height':label['height'],
                            'abs_width':label['width'],
                            'abs_x_min':label['left'],
                            'abs_y_min':label['top'],
                            'object_class_id':classes[int(label['type'])]
                        }
                        groundtruth.append(gt_label)

                # upload images and labels to mongodb
                upload_doc(image_name, image_ext, remote_path, width, height, plant, dataset, usecases, groundtruth)
                if (local_pc_home == db_pc_home):
                    copy_file(image_path, remote_path)
                else:
                    copy_file(image_path, local_path)
                # copy_file(image_path, local_path)



def upload_doc(iname, iext, ipath, iwidth, iheight, plant, datasets, usecases, gtdata, detections=None):

    query_plant = mydb.plants.find({'name':plant})
    plant_id = query_plant[0]['_id']

    usecase_ids = []
    for usecase in usecases:
        query_usecase = mydb.usecases.find({'name':usecase})
        usecase_ids.append(query_usecase[0]['_id'])
    dataset_ids = []
    for dataset in datasets:
        query_dataset = mydb.datasets.find({'name':dataset})
        # print(query_dataset[0])
        dataset_ids.append(query_dataset[0]['_id'])
    currentdoc = {
        'filename': iname,
        'extension': iext,
        'path': ipath,
        'width': iwidth,
        'height': iheight,
        'plant_id': plant_id,
        'usecase_ids': usecase_ids,
        'dataset_ids': dataset_ids,
        'ground_truth': gtdata,
        'detections': detections
    }

    # pprint(currentdoc)

    try:
        mydb.images.insert(currentdoc)
    except mongoerrors.PyMongoError as e:
        print(e)



def copy_file(path_to_file, new_path_to_file):

    if (os.path.isfile(path_to_file)):
        if (os.path.isfile(new_path_to_file)):
            print("File already exists")
        else:
            shutil.copyfile(path_to_file, new_path_to_file)


def check_if_exists(collection, field, value):
    # ! seems like we have to use .createIndex method and then insert the document
    doc = {
        field:value
    }


    mydb[collection].create_index(field, unique=True)
    mydb[collection].insert(doc)
    # .update({field:value},{field:value},upsert=True)



# def create_new_dataset()



###########################################

if __name__ == "__main__":
    main()
