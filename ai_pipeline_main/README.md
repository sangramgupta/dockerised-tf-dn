# AI Pipeline: Installation Instructions


## 1. Environment Variables:

Add the following environment variables to `.bashrc` file in home directory.

```
TF-related
Models-related
CUDA-related
```


## 2. Backend:

2.1. Create a virtual environment in `ai_pipeline_main` forlder

```
$ python3.5 -m venv ./.env
```

2.2. Activate the virtual environment

```
$ source .env/bin/activate
```

2.3. Install dependencies in the virtual environment

```
$ pip install ./requirements.txt
```

## 3. CUDA:

3.1. Add NVIDIA package repositories

```
$ wget https://developer.download.nvidia.com/compute/cuda/repos/ubuntu1804/x86_64/cuda-repo-ubuntu1804_10.0.130-1_amd64.deb
$ sudo dpkg -i cuda-repo-ubuntu1804_10.0.130-1_amd64.deb
$ sudo apt-key adv --fetch-keys https://developer.download.nvidia.com/compute/cuda/repos/ubuntu1804/x86_64/7fa2af80.pub
$ sudo apt-get update
$ wget http://developer.download.nvidia.com/compute/machine-learning/repos/ubuntu1804/x86_64/nvidia-machine-learning-repo-ubuntu1804_1.0.0-1_amd64.deb
$ sudo apt install ./nvidia-machine-learning-repo-ubuntu1804_1.0.0-1_amd64.deb
$ sudo apt-get update
```

3.2. Install NVIDIA driver
```
$ sudo apt-get install --no-install-recommends nvidia-driver-410
```

3.3. Reboot. Check that GPUs are visible

```
$ nvidia-smi
```

3.4. Install development and runtime libraries (~4GB)

```
$ sudo apt-get install --no-install-recommends \
    cuda-10-0 \
    libcudnn7=7.4.1.5-1+cuda10.0  \
    libcudnn7-dev=7.4.1.5-1+cuda10.0
```

## 4. Front-end:

4.1. Install npm dependencies in `webui/client` folder
```
$ npm install
```
4.2. Run Angular in `webui/client` folder

```
$ ng serve --o
```

## 5. MongoDB:

5.1. Boot up the Docker version of MongoDB in DevBox with IP: **10.180.142.120:27018**

```
$ ssh robotics@10.180.142.120
$ cd docker_mongodb
$ docker-compose up -d
```

_If the IP has changed, you can find the updated version in Slack: nextcloud channel._

## 6. TensorFlow:

6.1. Clone **Models** repo to home directory 

```
$ git clone https://github.com/tensorflow/models.git
```

6.2. Clone **Coco API** repo to home directory and make

```
$ git clone https://github.com/cocodataset/cocoapi.git
$ cd cocoapi/PythonAPI
$ make
$ cp -r pycocotools <path_to_models_repo>/research/
```

6.3. Protobuf Compilation

```
# From tensorflow/models/research/
$ protoc object_detection/protos/*.proto --python_out=.
```


6.4. Build and install TensorFlow Object Detection API in `models/research` folder

```
$ python setup.py build
$ python setup.py install
```

6.5. Run the test:

```
$ python object_detection/builders/model_builder_test.py
```

## 7. Tensorboard

7.1. Run Tensorboard (can be found under: localhost:6006)

```
$ tensorboard --logdir=/home/<user_name>/Deep_Learning/Trainings/<training_name>/backup
```

## Sources:

[Installation of TensorFlow Object Detection API](https://github.com/tensorflow/models/blob/master/research/object_detection/g3doc/installation.md)

[NVIDIA GPU Drivers and CUDA Setup](https://www.tensorflow.org/install/gpu)
