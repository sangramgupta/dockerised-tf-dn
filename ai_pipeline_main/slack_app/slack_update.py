from urllib import request, parse
import json
import socket
import sys

def send_message_to_slack(webhook, text):
    post = {"text": "{0}".format(text)}

    try:
        json_data = json.dumps(post)
        req = request.Request(webhook.strip('\n'),
                              data=json_data.encode('ascii'),
                              headers={'Content-Type': 'application/json'})
        resp = request.urlopen(req)
    except Exception as em:
        print("EXCEPTION: " + str(em))


e = open('devbox_name.txt', 'r')
devbox_name = (e.readline())
e = open('webhook.txt', 'r')
webhook = str(e.readline())

e = open('message.txt').read().splitlines()
message_post = '\n'.join(e)
if message_post != '':
    send_message_to_slack(webhook, message_post)
    e = open('message.txt', 'w')
    e.write('')
else:
    pass
