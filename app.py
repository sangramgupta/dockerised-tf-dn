from flask import Flask, request
import os
import subprocess

app = Flask(__name__)

UPLOAD_FOLDER = 'yolo-network/data/'
ALLOWED_EXTENSIONS = set(['jpg', 'jpeg'])

app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER

def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS

@app.route('/yolo-test/', methods=['POST'])
def get_image_save_and_process():
    if request.method == 'POST':
        if 'image' in request.files:
            file = request.files['image']
            file.save(os.path.join(app.config['UPLOAD_FOLDER'], file.filename))

            if file and allowed_file(file.filename):
                os.system('nvidia-smi')
                #os.system('./yolo-network/darknet detector test yolo-network/cfg/coco.data yolo-network/cfg/yolov3.cfg yolo-network/yolov3.weights yolo-network/data/'+file.filename)
                p = subprocess.Popen(['./yolo-network/darknet', 'detector', 'test', 'yolo-network/cfg/coco.data', 'yolo-network/cfg/darknet19.cfg', 'yolo-network/darknet19.weights', 'yolo-network/data/'+file.filename], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
                out, err = p.communicate()

            return str(out.decode("utf-8"))
    return 'Put an image mate'

#@app.route('/yolo-train/', methods=['POST'])
#def get_network_config_and_train():
#    if request.method == 'POST':
#        os.system('nvidia-smi')
#        os.system('./yolo-network/darknet detector train yolo-network/cfg/voc.data yolo-network/cfg/yolov3-voc.cfg yolo-network/darknet53.conv.74')


# @app.route('/imagenet-test/', methods=['POST'])



# @app.route('/imagenet-train/', methods=['POST'])



if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0', port=5021)
