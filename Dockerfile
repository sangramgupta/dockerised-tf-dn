############# Container Type #########################
FROM nvidia/cuda:10.0-cudnn7-devel-ubuntu16.04

############# Essentials ####################
RUN apt-get update

RUN apt-get install -y automake make libtool git wget autoconf build-essential unzip

RUN apt install -y vim

RUN apt-get update && apt-get install -y wget git && apt-get clean && rm -rf /var/cache/apt

RUN apt-get -y autoremove && apt-get -y autoclean

############# Python Upgrade ################
RUN apt-get install python3 -y && apt-get install python3-pip -y && pip3 install --upgrade pip

############ Redis Server ###################
RUN apt-get install redis-server -y && redis-server --daemonize yes

############ Python Libraries + Tensorflow ##################
RUN pip3 install Flask tensorflow-gpu Cython contextlib2 jupyter matplotlib lxml pillow redis mongoengine

########### Clear Cache ###################
RUN rm -rf /var/cache/apt

########### Copy Cloned Directory along with supporting directories inside ###############
COPY . /dockerised-tf-dn

############# Preparing Yolo Network ###################
WORKDIR /dockerised-tf-dn/yolo-network/

RUN make

######### Weights for Darknet Model ###################
#RUN wget https://pjreddie.com/media/files/yolov3.weights
#RUN wget https://pjreddie.com/media/files/darknet53.conv.74

############ Tensorflow CocoAPI ###############
WORKDIR /dockerised-tf-dn/cocoapi/PythonAPI/

RUN make

RUN cp -r pycocotools /dockerised-tf-dn/models/research/

############### Tensorflow Object Detection API #######################
WORKDIR /dockerised-tf-dn/models/research/

RUN wget -O protobuf.zip https://github.com/google/protobuf/releases/download/v3.0.0/protoc-3.0.0-linux-x86_64.zip

RUN unzip -o protobuf.zip

RUN ./bin/protoc object_detection/protos/*.proto --python_out=.

RUN export PYTHONPATH=$PYTHONPATH:`pwd`:`pwd`/slim

############### Setting Up Path for Object Detection API ###################
WORKDIR /dockerised-tf-dn/models/research/

RUN python3 setup.py build

RUN python3 setup.py install

################# Setting Up Path for Slim Library ###################
WORKDIR /dockerised-tf-dn/models/research/slim/

RUN python3 setup.py build

RUN python3 setup.py install

################ Run Shell Script ########################
RUN chmod +x /dockerised-tf-dn/env_scripts.sh

RUN /dockerised-tf-dn/env_scripts.sh
RUN echo '#export APP=/opt/tinyos-2.x/apps' >> /root/.bashrc
RUN cat /root/.bashrc
#################### Test for Object Detection API ##################
WORKDIR /dockerised-tf-dn/models/research/
CMD ["python3", "object_detection/builders/model_builder_test.py"]

###################### For Training ######################
#WORKDIR /dockerised-tf-dn/ai_pipeline_main/webui/server/
#CMD ["nohup","python3","training_worker.py","&"]

#################### API for Testing #######################
#WORKDIR /dockerised-tf-dn/
#CMD ["python3", "app.py"]

